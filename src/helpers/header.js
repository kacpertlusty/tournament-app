const header = token => ({
  headers: {
    Authorization: `bearer ${token}`,
  },
});

export default header;
