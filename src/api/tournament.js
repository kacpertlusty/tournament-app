import axios from 'axios';

const header = token => ({
  headers: {
    Authorization: `bearer ${token}`,
  },
});

const fetchAll = ({ token }) => axios
  .get('/api/tournament/', {
    headers: { Authorization: `bearer  ${token}` },
  })
  .then(res => res.data, error => error);

const create = (data, token) => axios
  .post(
    '/api/tournament/create',
    data,
    header(token),
  )
  .then(res => res.data, err => err);

const start = (id, token) => axios
  .post(
    `/api/tournament/${id}/first-round`,
    undefined,
    header(token),
  )
  .then(res => res.data, err => err);

const finishRound = (id, token, round) => axios
  .post(
    `/api/tournament/${id}/finish-round/${round}`,
    undefined,
    header(token),
  )
  .then(res => res.data, err => err);

const nextRound = (id, token) => axios
  .post(
    `/api/tournament/${id}/next-round`,
    undefined,
    header(token),
  )
  .then(res => res.data, err => err);

const deleteTournament = (id, token) => axios
  .delete(
    `/api/tournament/delete/${id}`,
    header(token),
  ).then(res => res.data, err => err);

const joinTournament = (id, token, data) => axios
  .post(
    `/api/tournament/${id}/register`,
    data,
    header(token),
  ).then(res => res.data, err => err);

const fetchSingleTournament = (id, token) => axios
  .get(
    `/api/tournament/?_id=${id}`,
    header(token),
  ).then(res => res.data, err => err);

const unregisterFromTournament = (id, token) => axios
  .post(
    `/api/tournament/${id}/unregister`,
    undefined,
    header(token),
  ).then(res => res.data, err => err);

const addScore = (id, playerId, score, token) => axios
  .post(
    `/api/tournament/${id}/add-score/${playerId}`,
    { score },
    header(token),
  ).then(res => res.data, err => err);

const update = (id, data, token) => axios
  .put(
    `/api/tournament/update/${id}`,
    data,
    header(token),
  ).then(res => res.data, err => err);

export default {
  addScore,
  create,
  deleteTournament,
  fetchAll,
  fetchSingleTournament,
  finishRound,
  joinTournament,
  nextRound,
  start,
  unregisterFromTournament,
  update,
};
