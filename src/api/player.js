import axios from 'axios';
import header from '../helpers/header';

const fetchPlayer = (id, token) => axios
  .get(
    `/api/user/${id}`,
    header(token),
  ).then(res => res.data, err => err);

export default { fetchPlayer };
