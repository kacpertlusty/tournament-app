import axios from 'axios';

const login = ({ email, password }) => axios
  .post('/api/user/login', { email, password })
  .then(result => result.data, error => error);

export default { login };
