import axios from 'axios';
import header from '../helpers/header';

const fetchMatches = token => axios
  .get(
    '/api/match/',
    header(token),
  )
  .then(result => result.data, error => error);

export default { fetchMatches };
