import axios from 'axios';
import header from '../helpers/header';

const fetchActiveGame = (id, token) => axios
  .get(
    `/api/match?player=${id}&finished=false`,
    header(token),
  )
  .then(result => result.data, error => error);

const finishActiveGame = (id, score, token) => axios
  .post(
    `/api/match/${id}/finish`,
    score,
    header(token),
  ).then(res => res.data, err => err);

export default { fetchActiveGame, finishActiveGame };
