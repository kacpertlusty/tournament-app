import axios from 'axios';
import header from '../helpers/header';

const fetch = token => axios
  .get(
    '/api/ranking/',
    header(token),
  ).then(res => res.data, err => err);

export default { fetch };
