/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListGroup from 'react-bootstrap/ListGroup';
import Table from 'react-bootstrap/Table';
import { RoundRow, PlayerRow, TournamentActions } from '../components';
import actions from '../redux/actions/tournaments';

const TournamentDetails = (props) => {
  const {
    dispatch,
    tournament,
    userId,
    token,
  } = props;
  if (!tournament) return (<></>);
  if (!tournament.players) tournament.players = [];
  return (
    <ListGroup variant="flush">
      <TournamentActions />
      <ListGroup.Item>{`Nazwa turnieju: ${tournament.name}`}</ListGroup.Item>
      <ListGroup.Item>{`Data: ${(new Date(tournament.date)).toDateString()}`}</ListGroup.Item>
      <ListGroup.Item>{`Miasto: ${tournament.city}`}</ListGroup.Item>
      <ListGroup.Item>{`Adres: ${tournament.street}, ${tournament.zipCode}`}</ListGroup.Item>
      <ListGroup.Item>{`Ilość graczy: ${tournament.players.length} / ${tournament.maxPlayers}`}</ListGroup.Item>
      <ListGroup.Item>{`Ilość rund: ${tournament.maxRounds}`}</ListGroup.Item>
      <ListGroup.Item>{`Zasady: ${tournament.rules}`}</ListGroup.Item>
      <ListGroup.Item>{`Opis: ${tournament.description}`}</ListGroup.Item>
      <ListGroup.Item>{`Ilość detachmentów: ${tournament.detachmentLimit}`}</ListGroup.Item>
      <ListGroup.Item>{`Termin rozpisek: ${tournament.detachmentLimit}`}</ListGroup.Item>
      <ListGroup.Item>{`Punkty karne za spóźnioną rozpiskę: ${tournament.penaltyForLateRoster}`}</ListGroup.Item>
      <Table>
        <thead>
          <tr>
            <th>Gracz</th>
            <th>Wynik z gier</th>
            <th>Punkty karne/bonusowe</th>
            <th>Suma punktów</th>
            <th>Akcje</th>
          </tr>
        </thead>
        <tbody>
          {
            tournament.players.map(player => (
              <PlayerRow
                onClick={score => dispatch(
                  actions.addScore(tournament._id, token, player.player._id, score),
                )}
                player={player}
                key={player._id}
                showActions={tournament.creator._id === userId}
              />
            ))
          }
        </tbody>
      </Table>
      {
        tournament.rounds.map(round => (
          <RoundRow round={round} key={round._id} />
        ))
      }
    </ListGroup>
  );
};

TournamentDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
  tournament: PropTypes.shape({
    name: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    rules: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    street: PropTypes.string.isRequired,
    zipCode: PropTypes.string.isRequired,
    maxRounds: PropTypes.number.isRequired,
    maxPlayers: PropTypes.number.isRequired,
    players: PropTypes.array.isRequired,
    detachmentLimit: PropTypes.number,
    rosterTimeLimit: PropTypes.string,
    penaltyForLateRoster: PropTypes.number,
    creator: PropTypes.shape({
      _id: PropTypes.string,
    }).isRequired,
  }).isRequired,
  userId: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  const tournament = state
    .tournaments
    .tournaments
    .filter(t => t._id === state.router.location.state)[0];
  const { _id, token } = state.login.user;
  if (tournament) {
    return {
      tournament,
      userId: _id,
      token,
    };
  }
  return {
    tournament: {
      creator: { },
    },
    userId: {},
    token: '',
  };
}

const connectedTournamentDetails = connect(mapStateToProps)(TournamentDetails);

export default connectedTournamentDetails;
