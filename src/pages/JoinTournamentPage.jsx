import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {
  LoadingModal,
  Modal,
  FormField,
  SelectForm,
} from '../components';
import { tournaments } from '../redux/actions';

const factions = [
  'Nie wybrano',
  'Adeptus Mechanicus',
  'Adeptus Ministorum',
  'Astra Militarum',
  'Astra Telepathica',
  'Asuryani',
  'Blood Angels',
  'Chaos Daemons',
  'Dark Angels',
  'Death Guard',
  'Deathwatch',
  'Drukhari',
  'Genestealer Cult',
  'Grey Knights',
  'Harlequins',
  'Heretic Astartes',
  'Inquisition',
  'Legion of the Damned',
  'Necron',
  'Officio Assassinorum',
  'Orks',
  'Questor Imperialis',
  'Questor Traitoris',
  'Sisters of Silence',
  'Space Marines',
  'Space Wolves',
  "T'au Empire",
  'Thousand Sons',
  'Tyranids',
  'Ynnari',
];

class JoinTournament extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tournamentData: {
        detachments: [
          { keyword: 'Brak', withWarlord: false },
          { keyword: 'Brak', withWarlord: false },
          { keyword: 'Brak', withWarlord: false },
        ],
        roster: '',
      },
      warlordCheckBoxes: [false, false, false],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDetachmentChange = this.handleDetachmentChange.bind(this);
    this.handleRosterChange = this.handleRosterChange.bind(this);
    this.handleWarlordChange = this.handleWarlordChange.bind(this);
  }

  handleSubmit() {
    const { dispatch, token, tournament } = this.props;
    const { tournamentData } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    dispatch(tournaments.joinTournament(tournament._id, token, tournamentData));
  }

  handleRosterChange(event) {
    const { tournamentData } = this.state;
    this.setState({
      tournamentData: {
        ...tournamentData,
        roster: event.target.value,
      },
    });
  }

  handleDetachmentChange(event) {
    const { tournamentData } = this.state;
    const { detachments } = tournamentData;
    detachments[event.target.name].keyword = event.target.value;
    this.setState({
      tournamentData: {
        ...tournamentData,
        detachments,
      },
    });
  }

  handleWarlordChange(event) {
    const { checked, name } = event.target;
    if (!checked) {
      return this.setState({
        warlordCheckBoxes: [false, false, false],
      });
    }
    const { tournamentData } = this.state;
    const { detachments } = tournamentData;
    const warlordCheckBoxes = [false, false, false];
    warlordCheckBoxes[name] = checked;
    for (let i = 0; i < 3; i += 1) {
      detachments[i].withWarlord = warlordCheckBoxes[i];
    }
    return this.setState({
      tournamentData: {
        ...tournamentData,
        detachments,
      },
      warlordCheckBoxes,
    });
  }

  render() {
    const { tournament } = this.props;
    const { tournamentData, warlordCheckBoxes } = this.state;
    return (
      <>
        <Modal
          show={false}
          onHide={() => {}}
          onClick={() => {}}
          headerText="Błąd"
          bodyText="Err"
          key="ErrorModal"
        />
        <LoadingModal
          key="LoadingModal"
          onHide={() => {}}
          show={false}
          text="Dołączanie do turnieju"
        />
        <Row className="justify-content-md-center">
          <Col sm="12">
            <Form style={{ paddingTop: '10px' }}>
              <FormField name="roster" onChange={this.handleRosterChange} value={tournamentData.roster} description="Rozpiska" key="roster" placeholder="Wklej swoją rozpiskę" />
              {
                [...Array(tournament.detachmentLimit).keys()].map(i => (
                  <div key={`div${i}`}>
                    <SelectForm
                      name={i}
                      options={factions}
                      description={`Frakcja detachmentu ${i + 1}`}
                      key={i}
                      onChange={this.handleDetachmentChange}
                    />
                    <Form.Group controlId="formBasicChecbox" key={`WarlordCheckBox${i}`}>
                      <Form.Check type="checkbox" name={i} label="Warlord" onChange={this.handleWarlordChange} checked={warlordCheckBoxes[i]} />
                    </Form.Group>
                  </div>
                ))
              }
              <br />
              <Button onClick={this.handleSubmit} variant="danger" block>Zarejestruj się</Button>
            </Form>
          </Col>
        </Row>
      </>
    );
  }
}

JoinTournament.propTypes = {
  dispatch: PropTypes.func.isRequired,
  tournament: PropTypes.shape({
    name: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    rules: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    street: PropTypes.string.isRequired,
    zipCode: PropTypes.string.isRequired,
    maxRounds: PropTypes.number.isRequired,
    maxPlayers: PropTypes.number.isRequired,
    players: PropTypes.array.isRequired,
    detachmentLimit: PropTypes.number.isRequired,
  }).isRequired,
  token: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
  const tournament = state
    .tournaments
    .tournaments
    // eslint-disable-next-line no-underscore-dangle
    .filter(t => t._id === state.router.location.state)[0];
  const { token } = state.login.user;
  if (tournament) return ({ token, tournament });
  return ({ tournament: {}, token });
};

const connectedJoinTournament = connect(mapStateToProps)(JoinTournament);
export default connectedJoinTournament;
