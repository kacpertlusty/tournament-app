import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import FormGroup from 'react-bootstrap/FormGroup';
import FormControl from 'react-bootstrap/FormControl';
import FormLabel from 'react-bootstrap/FormLabel';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { LoadingModal } from '../components';
import { login } from '../redux/actions';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  validateForm() {
    const { email, password } = this.state;
    return email.length > 0 && password.length > 0;
  }

  handleSubmit(event) {
    event.preventDefault();
    const { email, password } = this.state;
    if (email === '' || password === '') return;
    const { dispatch } = this.props;
    dispatch(login.login(email, password));
  }

  handleChange(event) {
    this.setState({
      [event.target.id]: event.target.value,
    });
  }

  render() {
    const {
      email,
      password,
    } = this.state;
    const { isLogging } = this.props;
    return (
      <>
        <LoadingModal
          onHide={() => {}}
          show={isLogging}
          text="Logowanie"
        />
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email">
            <FormLabel>Email</FormLabel>
            <FormControl
              autoFocus
              type="email"
              value={email}
              onChange={e => this.handleChange(e)}
            />
          </FormGroup>
          <FormGroup controlId="password">
            <FormLabel>Password</FormLabel>
            <FormControl
              value={password}
              onChange={e => this.handleChange(e)}
              type="password"
            />
          </FormGroup>
          <Button
            block
            disabled={!this.validateForm()}
            type="submit"
          >
            Login
          </Button>
        </form>
      </>
    );
  }
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isLogging: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  const { isLogging } = state.login;
  return {
    isLogging,
  };
}

const connectedLoginPage = connect(mapStateToProps)(Login);
export default connectedLoginPage;
