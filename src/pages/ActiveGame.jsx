import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import FormField from '../components/FormField';
import { activeGame } from '../redux/actions';
import LoadingModal from '../components/LoadingModal';

const validateScore = (val) => {
  let num = val;
  if (num > 20) {
    num = 20;
  } else if (num < 0) {
    num = 0;
  }
  return Number.parseInt(num, 10);
};

class ActiveGameClass extends Component {
  constructor(props) {
    super(props);
    const { game } = this.props;
    this.state = {
      scoreA: game.scoreA || 0,
      scoreB: game.scoreB || 0,
    };
    const { dispatch, id, token } = this.props;
    dispatch(activeGame.fetchActiveGame(id, token));
    this.setScoreA = this.setScoreA.bind(this);
    this.setScoreB = this.setScoreB.bind(this);
  }

  setScoreA(event) {
    let { value } = event.target;
    value = validateScore(value);
    this.setState({ scoreA: value });
  }

  setScoreB(event) {
    let { value } = event.target;
    value = validateScore(value);
    this.setState({ scoreB: value });
  }

  render() {
    const {
      dispatch,
      fetching,
      game,
      token,
    } = this.props;
    const { scoreA, scoreB } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    if (game === undefined || game._id === undefined || !game.playerA) {
      return (<>Brak aktywnej gry</>);
    }
    const playerA = game && game.playerA.player ? `${game.playerA.player.firstName} ${game.playerA.player.lastName}` : '';
    const playerB = game && game.playerB.player ? `${game.playerB.player.firstName} ${game.playerB.player.lastName}` : '';
    return (
      <>
        <LoadingModal show={fetching} text="Trwa pobieranie..." onHide={() => {}} />
        <ListGroup variant="flush">
          <ListGroup.Item>{`Gracz: ${playerA}`}</ListGroup.Item>
          <ListGroup.Item>
            <FormField
              description="Wynik gracza A"
              value={scoreA}
              placeholder="Wynik gracza A"
              onChange={this.setScoreA}
            />
          </ListGroup.Item>
          <ListGroup.Item>{`Gracz: ${playerB}`}</ListGroup.Item>
          <ListGroup.Item>
            <FormField
              description="Wynik gracza B"
              value={scoreB}
              placeholder="Wynik gracza B"
              onChange={this.setScoreB}
            />
          </ListGroup.Item>
          <ListGroup.Item>{`Stół: ${game && game.table}`}</ListGroup.Item>
          {
            !game.finished
            && (
              <Button
                variant="danger"
                onClick={() => dispatch(
                  // eslint-disable-next-line no-underscore-dangle
                  activeGame.finishGame(game._id, token, { scoreA, scoreB }),
                )}
              >
                Zgłoś wynik
              </Button>
            )
          }
        </ListGroup>
      </>
    );
  }
}

ActiveGameClass.propTypes = {
  game: PropTypes.shape({
    playerA: PropTypes.shape({
      player: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        mmr: PropTypes.number,
      }),
      score: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    }),
    playerB: PropTypes.shape({
      player: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        mmr: PropTypes.number,
      }),
      score: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    }),
    table: PropTypes.number,
  }).isRequired,
  id: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  fetching: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  fetching: state.activeGame.fetching,
  game: state.activeGame.activeGame,
  // eslint-disable-next-line no-underscore-dangle
  id: state.login.user._id,
  token: state.login.user.token,
});

const connectedActiveGame = connect(mapStateToProps)(ActiveGameClass);

export default connectedActiveGame;
