import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Modal,
  FormField,
  DateInput,
  LoadingModal,
  SelectForm,
} from '../components';
import { tournaments } from '../redux/actions';

import 'react-datepicker/dist/react-datepicker.css';

class CreateTournament extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tournament: {
        name: '',
        description: '',
        rules: '',
        city: '',
        street: '',
        maxPlayers: 20,
        maxRounds: 3,
        zipCode: '',
        date: new Date(),
        rank: 'Lokalny',
        detachmentLimit: 3,
        penaltyForLateRoster: 5,
        rosterTimeLimit: new Date(),
      },
      isCalendarOpen: false,
      isRosterCalendarOpen: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.toggleCalendar = this.toggleCalendar.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleRosterChange = this.handleRosterChange.bind(this);
    this.toggleRosterCalendar = this.toggleRosterCalendar.bind(this);
    this.handleCloseStatus = this.handleCloseStatus.bind(this);
    this.handleCreateCancel = this.handleCreateCancel.bind(this);
  }

  handleCloseStatus() {
    const { dispatch } = this.props;
    dispatch(tournaments.cancelCreateTournament());
  }

  validateForm() {
    return this;
  }

  handleSubmit() {
    const { tournament } = this.state;
    const { user, dispatch } = this.props;
    dispatch(tournaments.createTournament(tournament, user.token));
    return this;
  }

  handleChange(event) {
    const { tournament } = this.state;
    this.setState({
      tournament: {
        ...tournament,
        [event.target.name]: event.target.value,
      },
    });
  }

  toggleCalendar(e) {
    if (e) e.preventDefault();
    const { isCalendarOpen } = this.state;
    this.setState({ isCalendarOpen: !isCalendarOpen });
  }

  toggleRosterCalendar(e) {
    if (e) e.preventDefault();
    const { isRosterCalendarOpen } = this.state;
    this.setState({ isRosterCalendarOpen: !isRosterCalendarOpen });
  }

  handleDateChange(newDate) {
    const { tournament } = this.state;
    this.setState({ tournament: { ...tournament, date: newDate } });
    this.toggleCalendar();
  }

  handleRosterChange(newDate) {
    const { tournament } = this.state;
    this.setState({ tournament: { ...tournament, rosterTimeLimit: newDate } });
    this.toggleRosterCalendar();
  }

  handleCreateCancel() {
    const { dispatch } = this.props;
    dispatch(tournaments.cancelCreateTournament());
  }

  render() {
    const {
      isCalendarOpen,
      isRosterCalendarOpen,
      tournament,
    } = this.state;
    const { creating, fail } = this.props;
    return (
      <>
        <Modal
          show={fail !== undefined}
          onHide={this.handleCloseStatus}
          onClick={this.handleCloseStatus}
          headerText={fail && 'Błąd'}
          bodyText={fail && fail.message}
        />
        <LoadingModal
          key="LoadingModal"
          onHide={() => {}}
          show={creating}
          text="Dodawanie turnieju"
        />
        <Row className="justify-content-md-center">
          <Col sm="8">
            <Form style={{ paddingTop: '10px' }}>
              <FormField name="name" onChange={this.handleChange} value={tournament.name} description="Nazwa" placeholder="Wpisz nazwę turnieju" />
              <FormField name="description" value={tournament.description} onChange={this.handleChange} description="Opis" placeholder="Wpisz opis turnieju" />
              <FormField name="rules" onChange={this.handleChange} value={tournament.rules} description="Zasady" placeholder="Wpisz zasady" />
              <FormField name="city" onChange={this.handleChange} value={tournament.city} description="Miasto" placeholder="Wpisz miasto, np. Szczecin" />
              <FormField name="street" onChange={this.handleChange} value={tournament.street} description="Ulica" placeholder="Wpisz adres budynku, w którym odbędzie się turniej" />
              <FormField name="zipCode" onChange={this.handleChange} value={tournament.zipCode} description="Kod pocztowy" placeholder="Wpisz kod pocztowy, np 00-001" />
              <FormField name="maxPlayers" onChange={this.handleChange} value={tournament.maxPlayers} description="Maksymalna ilość graczy" placeholder="Wpisz maksymalna ilosc graczy turnieju" />
              <FormField name="maxRounds" onChange={this.handleChange} value={tournament.maxRounds} description="Ilość rund" placeholder="Ilość rund rozgrywanych na turnieju" />
              <FormField name="detachmentLimit" onChange={this.handleChange} value={tournament.detachmentLimit} description="Ilość detachmentów" placeholder="Maksymalna ilość detachmentów na turnieju" />
              <FormField name="penaltyForLateRoster" onChange={this.handleChange} value={tournament.penaltyForLateRoster} description="Punkty karne za spóźnioną rozpiskę" placeholder="Karne za spóźnioną rozpiskę które zostaną ODJĘTE od wyniku na koniec turnieju" />
              <DateInput
                date={tournament.date}
                onChange={this.handleDateChange}
                toggleCalendar={this.toggleCalendar}
                isCalendarOpen={isCalendarOpen}
                text="Data rozpoczęcia turnieju"
              />
              <DateInput
                date={tournament.rosterTimeLimit}
                onChange={this.handleRosterChange}
                toggleCalendar={this.toggleRosterCalendar}
                isCalendarOpen={isRosterCalendarOpen}
                text="Termin wysyłania rozpisek"
              />
              <SelectForm
                name="rank"
                options={['Lokalny', 'Master', 'Challanger', 'Liga']}
                onChange={this.handleChange}
              />
              <br />
              <Button onClick={this.handleSubmit} variant="primary" block>Utwórz turniej</Button>
            </Form>
          </Col>
        </Row>
      </>
    );
  }
}

CreateTournament.propTypes = {
  creating: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    token: PropTypes.string.isRequired,
  }).isRequired,
  fail: PropTypes.shape({
    message: PropTypes.string.isRequired,
  }),
};

CreateTournament.defaultProps = {
  fail: undefined,
};

function mapStateToProps(state) {
  const { user, logged } = state.login;
  const { creating, failCreate } = state.tournaments;
  return {
    user,
    logged,
    creating,
    fail: failCreate,
  };
}

const connectedCreateTournament = connect(mapStateToProps)(CreateTournament);
export default connectedCreateTournament;
