import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import {
  Modal,
  FormField,
  DateInput,
  LoadingModal,
} from '../components';
import { tournaments } from '../redux/actions';

class UpdateTournament extends Component {
  constructor(props) {
    super(props);
    const { tournament } = this.props;
    tournament.date = new Date(tournament.date);
    tournament.rosterTimeLimit = new Date(tournament.rosterTimeLimit);
    this.state = {
      isCalendarOpen: false,
      isRosterCalendarOpen: false,
      tournament,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.toggleCalendar = this.toggleCalendar.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleRosterChange = this.handleRosterChange.bind(this);
    this.toggleRosterCalendar = this.toggleRosterCalendar.bind(this);
    this.handleCloseStatus = this.handleCloseStatus.bind(this);
    this.handleCreateCancel = this.handleCreateCancel.bind(this);
  }

  handleCloseStatus() {
    const { dispatch } = this.props;
    dispatch(tournaments.cancelUpdate());
  }

  validateForm() {
    return this;
  }

  handleSubmit() {
    const { tournament } = this.state;
    const { user, dispatch } = this.props;
    dispatch(tournaments.updateTournament(tournament._id, tournament, user.token));
    return this;
  }

  handleChange(event) {
    const { tournament } = this.state;
    this.setState({
      tournament: {
        ...tournament,
        [event.target.name]: event.target.value,
      },
    });
  }

  toggleCalendar(e) {
    if (e) e.preventDefault();
    const { isCalendarOpen } = this.state;
    this.setState({ isCalendarOpen: !isCalendarOpen });
  }

  toggleRosterCalendar(e) {
    if (e) e.preventDefault();
    const { isRosterCalendarOpen } = this.state;
    this.setState({ isRosterCalendarOpen: !isRosterCalendarOpen });
  }

  handleDateChange(newDate) {
    const { tournament } = this.state;
    this.setState({ tournament: { ...tournament, date: newDate } });
    this.toggleCalendar();
  }

  handleRosterChange(newDate) {
    const { tournament } = this.state;
    this.setState({ tournament: { ...tournament, rosterTimeLimit: newDate } });
    this.toggleRosterCalendar();
  }

  handleCreateCancel() {
    const { dispatch } = this.props;
    dispatch(tournaments.cancelCreateTournament());
  }

  render() {
    const {
      tournament,
      isCalendarOpen,
      isRosterCalendarOpen,
    } = this.state;
    const { updating, updateError } = this.props;
    if (tournament.finished) return (<div />);
    return (
      <>
        <Modal
          show={updateError !== undefined}
          onHide={this.handleCloseStatus}
          onClick={this.handleCloseStatus}
          headerText={updateError && 'Błąd'}
          bodyText={updateError && updateError.message}
        />
        <LoadingModal
          key="LoadingModal"
          onHide={() => {}}
          show={updating}
          text="Aktualizowanie turnieju"
        />
        <Row className="justify-content-md-center">
          <Col sm="8">
            <Form style={{ paddingTop: '10px' }}>
              <FormField name="name" onChange={this.handleChange} value={tournament.name} description="Nazwa" placeholder="Wpisz nazwę turnieju" />
              <FormField name="description" value={tournament.description} onChange={this.handleChange} description="Opis" placeholder="Wpisz opis turnieju" />
              <FormField name="rules" onChange={this.handleChange} value={tournament.rules} description="Zasady" placeholder="Wpisz zasady" />
              <FormField name="city" onChange={this.handleChange} value={tournament.city} description="Miasto" placeholder="Wpisz miasto, np. Szczecin" />
              <FormField name="street" onChange={this.handleChange} value={tournament.street} description="Ulica" placeholder="Wpisz adres budynku, w którym odbędzie się turniej" />
              <FormField name="zipCode" onChange={this.handleChange} value={tournament.zipCode} description="Kod pocztowy" placeholder="Wpisz kod pocztowy, np 00-001" />
              <FormField name="maxPlayers" onChange={this.handleChange} value={tournament.maxPlayers} description="Maksymalna ilość graczy" placeholder="Wpisz maksymalna ilosc graczy turnieju" />
              <FormField name="maxRounds" onChange={this.handleChange} value={tournament.maxRounds} description="Ilość rund" placeholder="Ilość rund rozgrywanych na turnieju" />
              <DateInput
                date={tournament.date}
                onChange={this.handleDateChange}
                toggleCalendar={this.toggleCalendar}
                isCalendarOpen={isCalendarOpen}
              />
              <DateInput
                date={tournament.rosterTimeLimit}
                onChange={this.handleRosterChange}
                toggleCalendar={this.toggleRosterCalendar}
                isCalendarOpen={isRosterCalendarOpen}
              />
              <br />
              <Button onClick={this.handleSubmit} variant="primary" block>Utwórz turniej</Button>
            </Form>
          </Col>
        </Row>
      </>
    );
  }
}

UpdateTournament.propTypes = {
  dispatch: PropTypes.func.isRequired,
  tournament: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    rules: PropTypes.string,
    city: PropTypes.string,
    street: PropTypes.string,
    maxPlayers: PropTypes.number,
    maxRounds: PropTypes.number,
    zipCode: PropTypes.string,
    date: PropTypes.string,
    rank: PropTypes.string,
    detachmentLimit: PropTypes.number,
    penaltyForLateRoster: PropTypes.number,
    rosterTimeLimit: PropTypes.string,
    finished: PropTypes.bool,
  }),
  user: PropTypes.shape({
    token: PropTypes.string,
  }),
  updating: PropTypes.bool,
  updateError: PropTypes.shape({
    message: PropTypes.string,
  }),
};

UpdateTournament.defaultProps = {
  tournament: {
    name: '',
    description: '',
    rules: '',
    city: '',
    street: '',
    maxPlayers: 20,
    maxRounds: 3,
    zipCode: '',
    date: new Date(),
    rosterTimeLimit: new Date(),
  },
  user: {
    token: '',
  },
  updating: false,
  updateError: undefined,
};

const mapStateToProps = (state) => {
  const tournament = state
    .tournaments
    .tournaments
    .filter(t => t._id === state.router.location.state)[0];
  const { user } = state.login;
  const { updating, updateError } = state.tournaments;
  const {
    name,
    description,
    rules,
    city,
    street,
    maxPlayers,
    maxRounds,
    zipCode,
    date,
    rosterTimeLimit,
    _id,
  } = tournament;
  return ({
    user,
    tournament: {
      name,
      description,
      rules,
      city,
      street,
      maxPlayers,
      maxRounds,
      zipCode,
      date,
      rosterTimeLimit,
      _id,
    },
    updating,
    updateError,
  });
};

const connectedUpdateTournament = connect(mapStateToProps)(UpdateTournament);
export default connectedUpdateTournament;
