import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/Table';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';

import { tournaments as tournamentActions } from '../redux/actions';
import { LoadingModal, Modal, TournamentRow } from '../components';

class ShowTournaments extends Component {
  constructor(props) {
    super(props);
    const { token, dispatch } = this.props;
    dispatch(tournamentActions.fetchTournaments(token));
    this.showDetails = this.showDetails.bind(this);
  }

  showDetails(tournament) {
    const { dispatch } = this.props;
    dispatch(push('/tournamentDetails', tournament._id));
  }

  render() {
    const {
      tournaments,
      fetching,
      fail,
      dispatch,
      token,
    } = this.props;
    return (
      <>
        <LoadingModal
          show={fetching && tournaments.length === 0}
          onHide={() => {}}
          text="Wczytywanie turniejów"
        />
        <Modal
          bodyText={fail.message || ''}
          headerText="Błąd"
          key="errorModal"
          onClick={() => {
            dispatch(tournamentActions.fetchTournaments(token));
          }}
          onHide={() => {}}
          show={fail.message !== undefined}
        />
        <Table responsive key="Table">
          <thead>
            <tr>
              <th>Nazwa</th>
              <th>Opis</th>
              <th>Miejsce</th>
              <th>Miejsca</th>
              <th>Ranga</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {
              tournaments.map(tournament => (
                <TournamentRow
                  tournament={tournament}
                  onClick={() => this.showDetails(tournament)}
                  key={tournament.name}
                />
              ))
            }
          </tbody>
        </Table>
      </>
    );
  }
}

ShowTournaments.propTypes = {
  tournaments: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    city: PropTypes.string,
  })).isRequired,
  dispatch: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  fail: PropTypes.shape({
    message: PropTypes.string,
  }),
  fetching: PropTypes.bool.isRequired,
};

ShowTournaments.defaultProps = {
  fail: { message: undefined },
};

function mapStateToProps(state) {
  const { token } = state.login.user;
  const { tournaments, fetching, fail } = state.tournaments;
  return {
    token,
    tournaments,
    fetching,
    fail,
  };
}

const connectedShowTournamentsPage = connect(mapStateToProps)(ShowTournaments);
export default connectedShowTournamentsPage;
