/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Table from 'react-bootstrap/Table';
import { MatchRow, LoadingModal, Modal } from '../components';
import { matches as matchesActions } from '../redux/actions';

class MatchHistoryPage extends Component {
  constructor(props) {
    super(props);
    const { dispatch, token } = this.props;
    dispatch(matchesActions.fetchMatches(token));
  }

  render() {
    const {
      dispatch,
      error,
      fetching,
      matches,
      token,
    } = this.props;
    return (
      <>
        <LoadingModal
          key="fetchingLoadingModal"
          text="trwa pobieranie historii gier"
          show={fetching}
        />
        <Modal
          bodyText={error.message || ''}
          headerText="Błąd"
          key="errorModal"
          onClick={() => {
            dispatch(matchesActions.fetchMatches(token));
          }}
          onHide={() => {}}
          show={error && error.message !== undefined}
        />
        <Table responsive key="Table">
          <thead>
            <tr>
              <th>Gracz A</th>
              <th>Wynik A</th>
              <th>Gracz B</th>
              <th>Wynik B</th>
              <th>Stół</th>
              <th>Skończony</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {
              matches.map(match => (
                <MatchRow
                  match={match}
                  onClick={() => dispatch(push('/matchDetails', match._id))}
                  // eslint-disable-next-line no-underscore-dangle
                  key={`match:${match._id}`}
                />
              ))
            }
          </tbody>
        </Table>
      </>
    );
  }
}

MatchHistoryPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  fetching: PropTypes.bool,
  matches: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string,
    playerA: PropTypes.shape({
      player: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        detachments: PropTypes.arrayOf(PropTypes.shape({
          keyword: PropTypes.string,
        })),
      }),
      score: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]),
    }),
    playerB: PropTypes.shape({
      player: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        detachments: PropTypes.arrayOf(PropTypes.shape({
          keyword: PropTypes.string,
        })),
      }),
      score: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]),
    }),
  })).isRequired,
  token: PropTypes.string.isRequired,
  error: PropTypes.shape({
    message: PropTypes.string,
  }),
};

MatchHistoryPage.defaultProps = {
  fetching: false,
  error: { message: undefined },
};

const mapStateToProps = (state) => {
  const { fetching, matches } = state.matches;
  const { token } = state.login.user;
  return ({
    fetching,
    matches,
    token,
  });
};

const connectedMatchHistoryPage = connect(mapStateToProps)(MatchHistoryPage);
export default connectedMatchHistoryPage;
