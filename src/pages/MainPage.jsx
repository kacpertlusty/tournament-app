import React from 'react';
import { connect } from 'react-redux';
import Container from 'react-bootstrap/Container';
import PropTypes from 'prop-types';

const MainPage = (props) => {
  const { user } = props;
  return (
    <Container>
      <h1>Witaj!</h1>
      <div>{user.firstName}</div>
      <div>{user.lastName}</div>
    </Container>
  );
};

MainPage.propTypes = {
  user: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.login.user,
  };
}
const connectedMainPage = connect(mapStateToProps)(MainPage);
export default connectedMainPage;
