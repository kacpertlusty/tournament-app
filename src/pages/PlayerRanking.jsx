import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { push } from 'connected-react-router';
import { LoadingModal } from '../components';
import actions from '../redux/actions/ranking';
import actionsPlayer from '../redux/actions/player';

const PlayerRanking = (props) => {
  const {
    dispatch,
    fetching,
    players,
    token,
  } = props;
  if (players.length === 0) dispatch(actions.fetchRanking(token));
  return (
    <>
      <LoadingModal
        key="LoadingModal"
        onHide={() => {}}
        show={fetching}
        text="Pobieranie rankingu..."
      />
      <Table responsive="sm">
        <thead>
          <tr>
            <th>Imie</th>
            <th>Nazwisko</th>
            <th>MMR</th>
            <th>Akcje</th>
          </tr>
        </thead>
        <tbody>
          {
            players.map(player => (
              <tr key={player._id}>
                <th>{player.firstName}</th>
                <th>{player.lastName}</th>
                <th>{player.mmr}</th>
                <th>
                  <Button
                    onClick={() => {
                      dispatch(actionsPlayer.fetch(player._id, token));
                      dispatch(push('/playerDetails'));
                    }}
                  >
                    Pokaz profil
                  </Button>
                </th>
              </tr>
            ))
          }
        </tbody>
      </Table>
    </>
  );
};

PlayerRanking.propTypes = {
  players: PropTypes.arrayOf(PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    mmr: PropTypes.number,
    _id: PropTypes.string,
  })),
  fetching: PropTypes.bool,
  token: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
};

PlayerRanking.defaultProps = {
  players: [],
  fetching: false,
};

const mapStateToProps = (state) => {
  const { players } = state.ranking;
  const { token } = state.login.user;
  return { players, token };
};

const connectedPlayerRanking = connect(mapStateToProps)(PlayerRanking);
export default connectedPlayerRanking;
