/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListGroup from 'react-bootstrap/ListGroup';

const MatchDetails = (props) => {
  const { match } = props;
  if (Object.keys(match).length === 0) return (<></>);
  return (
    <ListGroup variant="flush">
      <ListGroup.Item key="PlayerAName">{`Gracz ${match.playerA.player.firstName} ${match.playerA.player.lastName}`}</ListGroup.Item>
      <ListGroup.Item key="PlayerAScore">{`Wynik ${match.playerA.score}`}</ListGroup.Item>
      {
        match.playerA.detachments.map(detachment => (
          detachment.keyword === 'Brak' ? <div key={detachment._id} />
            : <ListGroup.Item key={detachment._id}>{`Detachment ${detachment.keyword} ${detachment.warlord ? 'warlord' : ''}`}</ListGroup.Item>
        ))
      }
      <ListGroup.Item key="PlayerBName">{`Gracz ${match.playerB.player.firstName} ${match.playerB.player.lastName}`}</ListGroup.Item>
      <ListGroup.Item key="PlayerBScore">{`Wynik ${match.playerB.score}`}</ListGroup.Item>
      {
        match.playerB.detachments.map(detachment => (
          detachment.keyword === 'Brak' ? <div key={detachment._id} />
            : <ListGroup.Item key={detachment._id}>{`Detachment ${detachment.keyword} ${detachment.warlord ? 'warlord' : ''}`}</ListGroup.Item>
        ))
      }
      <ListGroup.Item key="Table">{`Stół ${match.table}`}</ListGroup.Item>
      <ListGroup.Item key="Tournament">{`Turniej ${match.tournament.name}`}</ListGroup.Item>
      <ListGroup.Item key="IsFinished">{`Stan: ${match.finished ? 'zakończony' : 'aktywny'}`}</ListGroup.Item>
    </ListGroup>
  );
};

MatchDetails.propTypes = {
  match: PropTypes.shape({
    finished: PropTypes.bool,
    table: PropTypes.number,
    tournament: PropTypes.shape({
      name: PropTypes.string,
    }),
    playerA: PropTypes.shape({
      player: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        mmr: PropTypes.number,
      }),
      score: PropTypes.number,
      detachments: PropTypes.arrayOf(PropTypes.shape({
        keyword: PropTypes.string,
        withWarlod: PropTypes.bool,
      })),
    }),
    playerB: PropTypes.shape({
      player: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        mmr: PropTypes.number,
      }),
      score: PropTypes.number,
      detachments: PropTypes.arrayOf(PropTypes.shape({
        keyword: PropTypes.string,
        withWarlod: PropTypes.bool,
      })),
    }),
  }).isRequired,
};

const mapStateToProps = (state) => {
  const matchId = state.router.location.state;
  const match = state
    .matches
    .matches
    .filter(m => m._id === matchId)[0];
  return { match };
};

const connectedMatchDetails = connect(mapStateToProps)(MatchDetails);

export default connectedMatchDetails;
