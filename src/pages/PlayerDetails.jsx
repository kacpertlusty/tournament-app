import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListGroup from 'react-bootstrap/ListGroup';
import Table from 'react-bootstrap/Table';

const renderDetachment = (detachment, i) => {
  const { keyword, withWarlord } = detachment;
  if (keyword === 'Brak') return <th key={i} />;
  if (withWarlord) {
    return (
      <th key={i}><b>{keyword}</b></th>
    );
  }
  return <th key={i}>{keyword}</th>;
};

const PlayerDetails = (props) => {
  const { player } = props;
  return (
    <ListGroup variant="flush">
      <ListGroup.Item>{`Imię: ${player.firstName}`}</ListGroup.Item>
      <ListGroup.Item>{`Nazwisko: ${player.lastName}`}</ListGroup.Item>
      <ListGroup.Item>{`MMR: ${player.mmr}`}</ListGroup.Item>
      <Table>
        <thead>
          <tr>
            <th>Nazwa turnieju</th>
            <th>Wynik</th>
            <th>Detachments</th>
            <th />
            <th />
          </tr>
        </thead>
        <thead>
          {
            player.tournaments.map(tournament => (
              <tr key={tournament.name}>
                <th>{tournament.name}</th>
                <th>{tournament.players[0].score + tournament.players[0].bonusScore}</th>
                {
                  tournament.players[0].detachments.map((d, i) => renderDetachment(d, i))
                }
              </tr>
            ))
          }
        </thead>
      </Table>
    </ListGroup>
  );
};

PlayerDetails.propTypes = {
  player: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    mmr: PropTypes.number,
    id: PropTypes.string,
    tournaments: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      players: PropTypes.arrayOf(PropTypes.shape({
        score: PropTypes.number,
        bonusScore: PropTypes.number,
        detachments: PropTypes.arrayOf(PropTypes.shape({
          keyword: PropTypes.string,
          withWarlod: PropTypes.bool,
        })),
      })),
    })),
  }),
};

PlayerDetails.defaultProps = {
  player: {
    firstName: '',
    lastName: '',
    mmr: 0,
    id: '',
    tournaments: [],
  },
};

const mapStateToProps = state => ({
  player: state.player.player,
});

const connectedPlayerDetails = connect(mapStateToProps)(PlayerDetails);
export default connectedPlayerDetails;
