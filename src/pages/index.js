export { default as ActiveGame } from './ActiveGame';
export { default as CreateTournament } from './CreateTournament';
export { default as JoinTournament } from './JoinTournamentPage';
export { default as Login } from './Login';
export { default as MainPage } from './MainPage';
export { default as MatchDetails } from './MatchDetails';
export { default as MatchHistory } from './MatchHistory';
export { default as PlayerDetails } from './PlayerDetails';
export { default as PlayerRanking } from './PlayerRanking';
export { default as ShowTournaments } from './ShowTournaments';
export { default as TournamentDetails } from './TournamentDetails';
export { default as UpdateTournament } from './UpdateTournament';
