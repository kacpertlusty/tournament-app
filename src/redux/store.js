import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';
import rootReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger();

export const history = createBrowserHistory();

export default createStore(
  rootReducer(history), // root reducer with router state
  applyMiddleware(
    routerMiddleware(history), // for dispatching history actions
    sagaMiddleware,
    loggerMiddleware,
  ),
);

sagaMiddleware.run(rootSaga);
