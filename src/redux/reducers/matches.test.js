import actionTypes from '../actionTypes/matchesActionTypes';
import reducer from './matches';

describe('matches reducer test suite', () => {
  const initialState = {
    error: undefined,
    fetched: false,
    fetching: false,
    matches: [],
  };

  it('should return initial state if no action is provided', () => {
    expect(reducer()).toEqual(initialState);
  });

  it('should reset error and fetched and set fetching on request action', () => {
    expect(reducer({
      ...initialState,
      error: new Error('fake err'),
      fetched: true,
    }, {
      type: actionTypes.FETCH_MATCHES_REQUEST,
    })).toEqual({
      error: undefined,
      fetched: false,
      fetching: true,
      matches: [],
    });
  });

  it('should set matches and fetched and reset fetching on success action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
    }, {
      type: actionTypes.FETCH_MATCHES_SUCCESS,
      matches: ['fake', 'arr'],
    })).toEqual({
      fetched: true,
      fetching: false,
      error: undefined,
      matches: ['fake', 'arr'],
    });
  });

  it('should set error and reset fetching on failure action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
    }, {
      type: actionTypes.FETCH_MATCHES_FAILURE,
      error: new Error('fake error'),
    })).toEqual({
      fetched: true,
      fetching: false,
      error: new Error('fake error'),
      matches: [],
    });
  });

  it('should reset fetching and fetched on cancel action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
      fetched: true,
    }, {
      type: actionTypes.FETCH_MATCHES_CANCEL,
    })).toEqual({
      fetching: false,
      fetched: false,
      error: undefined,
      matches: [],
    });
  });
});
