import actionTypes from '../actionTypes/player';
import reducer from './player';

describe('player reducer test suite', () => {
  const initialState = {
    fetching: false,
    player: {},
    error: undefined,
  };

  it('should return initial state if no action is provided', () => {
    expect(reducer()).toEqual(initialState);
  });

  it('should set fetching and clear error on request action', () => {
    expect(reducer({
      ...initialState,
      error: new Error('fake error'),
    }, {
      type: actionTypes.FETCH_PLAYER_REQUEST,
    })).toEqual({
      fetching: true,
      player: {},
      error: undefined,
    });
  });

  it('should set player and reset fetching on success action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
    }, {
      type: actionTypes.FETCH_PLAYER_SUCCESS,
      player: { fake: 'obj' },
    })).toEqual({
      fetching: false,
      error: undefined,
      player: { fake: 'obj' },
    });
  });

  it('should set error and reset fetching and user on failure action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
      player: { fake: 'err' },
    }, {
      type: actionTypes.FETCH_PLAYER_FAILURE,
      error: new Error('fake error'),
    })).toEqual({
      fetching: false,
      player: {},
      error: new Error('fake error'),
    });
  });

  it('should reset error and fetching on cancel action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
      error: new Error('fake error'),
    }, {
      type: actionTypes.FETCH_PLAYER_CANCEL,
    })).toEqual({
      fetching: false,
      error: undefined,
    });
  });
});
