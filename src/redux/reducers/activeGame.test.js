import actionTypes from '../actionTypes/activeGameActionTypes';
import reducer from './activeGame';

describe('active game reducer test suite', () => {
  const initialState = {
    fetching: false,
    activeGame: {},
    error: undefined,
    finishing: false,
  };

  it('should return initial state when no action is given', () => {
    expect(reducer()).toEqual(initialState);
  });

  it('should set fetching and reset error on request action', () => {
    expect(reducer({
      ...initialState,
      error: new Error('fake err'),
    }, {
      type: actionTypes.FETCH_ACTIVE_GAME_REQUEST,
    })).toEqual({
      fetching: true,
      error: undefined,
      finishing: false,
      activeGame: {},
    });
  });

  it('should set activeGame and reset fetching', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
    }, {
      type: actionTypes.FETCH_ACTIVE_GAME_SUCCESS,
      activeGame: { fake: 'id' },
    })).toEqual({
      fetching: false,
      error: undefined,
      activeGame: { fake: 'id' },
      finishing: false,
    });
  });

  it('should set error and reset fetching and activeGame', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
      activeGame: { fake: 'id' },
    }, {
      type: actionTypes.FETCH_ACTIVE_GAME_FAILURE,
      error: new Error('fake error'),
    })).toEqual({
      fetching: false,
      error: new Error('fake error'),
      activeGame: {},
      finishing: false,
    });
  });

  it('should reset fetching, activeGame and error', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
      activeGame: { fake: 'id' },
      error: new Error('fake error'),
    }, {
      type: actionTypes.FETCH_ACTIVE_GAME_CANCEL,
    })).toEqual({
      fetching: false,
      error: undefined,
      activeGame: { },
      finishing: false,
    });
  });

  it('should set fetching and reset error on request action', () => {
    expect(reducer({
      ...initialState,
      error: new Error('fake err'),
    }, {
      type: actionTypes.FINISH_ACTIVE_GAME_REQUEST,
    })).toEqual({
      fetching: false,
      error: undefined,
      finishing: true,
      activeGame: {},
    });
  });

  it('should set activeGame and reset fetching', () => {
    expect(reducer({
      ...initialState,
      finishing: true,
    }, {
      type: actionTypes.FINISH_ACTIVE_GAME_SUCCESS,
      activeGame: { fake: 'id' },
    })).toEqual({
      fetching: false,
      error: undefined,
      activeGame: { fake: 'id' },
      finishing: false,
    });
  });

  it('should set error and reset fetching and activeGame', () => {
    expect(reducer({
      ...initialState,
      finishing: true,
      activeGame: { fake: 'id' },
    }, {
      type: actionTypes.FINISH_ACTIVE_GAME_FAILURE,
      error: new Error('fake error'),
    })).toEqual({
      fetching: false,
      error: new Error('fake error'),
      activeGame: {},
      finishing: false,
    });
  });

  it('should reset fetching, activeGame and error', () => {
    expect(reducer({
      ...initialState,
      finishing: true,
      error: new Error('fake error'),
    }, {
      type: actionTypes.FINISH_ACTIVE_GAME_CANCEL,
    })).toEqual({
      fetching: false,
      error: undefined,
      activeGame: { },
      finishing: false,
    });
  });
});
