import actionTypes from '../actionTypes/ranking';

const initialState = {
  players: [],
  fetching: false,
  error: undefined,
};

const rankingReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.FETCH_RANKING_REQUEST:
      return {
        ...state,
        fetching: true,
        error: undefined,
      };
    case actionTypes.FETCH_RANKING_SUCCESS:
      return {
        ...state,
        fetching: false,
        players: action.players,
      };
    case actionTypes.FETCH_RANKING_FAILURE:
      return {
        ...state,
        fetching: false,
        error: action.error,
      };
    case actionTypes.FETCH_RANKING_CANCEL:
      return {
        ...state,
        fetching: false,
        error: undefined,
      };
    default:
      return { ...state };
  }
};

export default rankingReducer;
