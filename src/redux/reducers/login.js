import actionTypes from '../actionTypes';

const initialState = {
  user: { },
  logged: false,
  isLogging: false,
};

const loginReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.LOGIN_REQUEST:
      return { ...state, isLogging: true };
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        user: action.user,
        logged: action.logged,
        isLogging: false,
      };
    case actionTypes.LOGIN_FAILURE:
      return {
        ...state,
        user: {},
        logged: false,
        isLogging: false,
      };
    case actionTypes.LOGOUT:
      return {
        ...state, logged: false,
      };
    default:
      return state;
  }
};
export default loginReducer;
