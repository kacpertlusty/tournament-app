import actionTypes from '../actionTypes/ranking';
import reducer from './ranking';

describe('Ranking reducer test suite', () => {
  const initialState = {
    players: [],
    fetching: false,
    error: undefined,
  };

  it('should return default initial state', () => {
    expect(reducer()).toEqual(initialState);
  });

  it('should set fetching and reset error on request action', () => {
    expect(reducer({
      ...initialState,
      error: new Error('fake error'),
    }, {
      type: actionTypes.FETCH_RANKING_REQUEST,
    })).toEqual({
      players: [],
      fetching: true,
      error: undefined,
    });
  });

  it('should set players and clear fetching on success action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
    }, {
      type: actionTypes.FETCH_RANKING_SUCCESS,
      players: ['fake', 'arr'],
    })).toEqual({
      players: ['fake', 'arr'],
      fetching: false,
      error: undefined,
    });
  });

  it('should set error and clear fetching on failure action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
    }, {
      type: actionTypes.FETCH_RANKING_FAILURE,
      error: new Error('fake error'),
    })).toEqual({
      players: [],
      fetching: false,
      error: new Error('fake error'),
    });
  });

  it('should clear fetching and error on cancel action', () => {
    expect(reducer({
      ...initialState,
      fetching: true,
      error: new Error('fake error'),
    }, {
      type: actionTypes.FETCH_RANKING_CANCEL,
    })).toEqual({
      players: [],
      fetching: false,
      error: undefined,
    });
  });
});
