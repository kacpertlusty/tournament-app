import reducer from './tournaments';
import actionTypes from '../actionTypes';

describe('Tournament reducer test suite', () => {
  it('should return default state', () => {
    expect(reducer()).toEqual({
      tournaments: [],
      fetching: false,
      creating: false,
      cancelCreateCreate: false,
      cancelFetch: false,
      failFetch: undefined,
      failCreate: undefined,
      finishedFinishRound: false,
      cancelFinishRound: false,
      finishingRound: false,
      startingNextRound: false,
      failNextRound: undefined,
      cancelNextRound: false,
      startedNextRound: false,
      deletingTournament: false,
      errorDeleteTournament: undefined,
      joiningTournament: false,
      joinTournamentCancel: false,
      joinTournamentMessage: '',
      errorJoinTournament: undefined,
      unregistering: false,
      unregisterCancel: false,
      unregisterMessage: '',
      errorUnregister: undefined,
      addingScore: false,
      addScoreError: undefined,
      updating: false,
      updateError: undefined,
    });
  });
  it('action: FETCH_TOURNAMENTS_REQUEST should set fetching and cancel fetch', () => {
    expect(reducer({
      fakeProp: 'fake_val',
    }, {
      type: actionTypes.FETCH_TOURNAMENTS_REQUEST,
    })).toEqual({
      fakeProp: 'fake_val',
      cancelFetch: false,
      fetching: true,
    });
  });
  it('action: FETCH_TOURNAMENTS_SUCCESS', () => {
    expect(reducer({
      fakeProp: 'fake_val',
    }, {
      type: actionTypes.FETCH_TOURNAMENTS_SUCCESS,
      tournaments: ['fake_1', 'fake_2'],
    })).toEqual({
      fakeProp: 'fake_val',
      tournaments: ['fake_1', 'fake_2'],
      fetching: false,
      cancelFetch: false,
    });
  });
  it('action: FETCH_TOURNAMENTS_FAILURE', () => {
    expect(reducer({
      fakeProp: 'fake',
    }, {
      type: actionTypes.FETCH_TOURNAMENTS_FAILURE,
      fail: new Error('fake err'),
    })).toEqual({
      fakeProp: 'fake',
      fetching: false,
      failFetch: new Error('fake err'),
    });
  });
  it('action: CREATE_TOURNAMENT_REQUEST', () => {
    expect(reducer({
      fakeProp: 'fake',
    }, {
      type: actionTypes.CREATE_TOURNAMENT_REQUEST,
    })).toEqual({
      creating: true,
      cancelCreate: false,
      failCreate: undefined,
      fakeProp: 'fake',
    });
  });
  it('action: CREATE_TOURNAMENT_SUCCESS', () => {
    expect(reducer({
      tournaments: ['first_tournament'],
    }, {
      type: actionTypes.CREATE_TOURNAMENT_SUCCESS,
      tournament: 'new_tournament',
    })).toEqual({
      tournaments: ['first_tournament', 'new_tournament'],
      creating: false,
      cancelCreate: false,
      failCreate: undefined,
    });
  });
  it('action: CREATE_TOURNAMENT_FAILURE', () => {
    expect(reducer({
      tournaments: ['tournament'],
    }, {
      type: actionTypes.CREATE_TOURNAMENT_FAILURE,
      fail: new Error('fake err'),
    })).toEqual({
      tournaments: ['tournament'],
      creating: false,
      failCreate: new Error('fake err'),
    });
  });
  it('action: CREATE_TOURNAMENT_CANCEL', () => {
    expect(reducer({
      tournaments: ['fake'],
    }, {
      type: actionTypes.CREATE_TOURNAMENT_CANCEL,
    })).toEqual({
      creating: false,
      cancelCreate: true,
      failCreate: undefined,
      tournaments: ['fake'],
    });
  });
  it('action: START_TOURNAMENT_REQUEST', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.START_TOURNAMENT_REQUEST,
    })).toEqual({
      starting: true,
      cancelStart: false,
      failStart: undefined,
      finishedStarting: false,
      tournaments: [],
    });
  });
  it('action: START_TOURNAMENT_SUCCESS', () => {
    expect(reducer({
      tournaments: [{ _id: 1, val: 'd' }, { _id: 2, val: 'c' }],
    }, {
      type: actionTypes.START_TOURNAMENT_SUCCESS,
      tournament: { _id: 2, val: 'new_val' },
    })).toEqual({
      starting: false,
      finishedStarting: true,
      tournaments: [{ _id: 1, val: 'd' }, { _id: 2, val: 'new_val' }],
    });
  });
  it('action: START_TOURNAMENT_FAILURE', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.START_TOURNAMENT_FAILURE,
      fail: new Error('fake_err'),
    })).toEqual({
      starting: false,
      finishedStarting: true,
      failStart: new Error('fake_err'),
      tournaments: [],
    });
  });
  it('action: START_TOURNAMENT_CANCEL', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.START_TOURNAMENT_CANCEL,
    })).toEqual({
      tournaments: [],
      starting: false,
      cancelStart: false,
    });
  });
  it('action: FINISH_ROUND_TOURNAMENT_REQUEST', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.FINISH_ROUND_TOURNAMENT_REQUEST,
    })).toEqual({
      tournaments: [],
      finishingRound: true,
      cancelFinishRound: false,
      failFinishRound: undefined,
      finishedFinishRound: false,
    });
  });
  it('action: FINISH_ROUND_TOURNAMENT_SUCCESS', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.FINISH_ROUND_TOURNAMENT_SUCCESS,
    })).toEqual({
      finishedFinishRound: true,
      finishingRound: false,
      tournaments: [],
    });
  });
  it('action: FINISH_ROUND_TOURNAMENT_FAILURE', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.FINISH_ROUND_TOURNAMENT_FAILURE,
      fail: new Error('fake err'),
    })).toEqual({
      tournaments: [],
      failFinishRound: new Error('fake err'),
      finishedFinishRound: true,
      finishingRound: false,
    });
  });
  it('action: FINISH_ROUND_TOURNAMENT_CANCEL', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.FINISH_ROUND_TOURNAMENT_CANCEL,
    })).toEqual({
      tournaments: [],
      finishingRound: false,
      cancelFinishRound: true,
    });
  });
  it('action: NEXT_ROUND_TOURNAMENT_REQUEST', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.NEXT_ROUND_TOURNAMENT_REQUEST,
    })).toEqual({
      startingNextRound: true,
      failNextRound: undefined,
      cancelNextRound: false,
      startedNextRound: false,
      tournaments: [],
    });
  });
  it('action: NEXT_ROUND_TOURNAMENT_SUCCESS', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.NEXT_ROUND_TOURNAMENT_SUCCESS,
    })).toEqual({
      tournaments: [],
      startingNextRound: false,
      startedNextRound: true,
    });
  });
  it('action: NEXT_ROUND_TOURNAMENT_FAILURE', () => {
    expect(reducer({
      tournaments: [],
    }, {
      fail: new Error('fake err'),
      type: actionTypes.NEXT_ROUND_TOURNAMENT_FAILURE,
    })).toEqual({
      tournaments: [],
      startingNextRound: false,
      failNextRound: new Error('fake err'),
    });
  });
  it('action: NEXT_ROUND_TOURNAMENT_CANCEL', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.NEXT_ROUND_TOURNAMENT_CANCEL,
    })).toEqual({
      tournaments: [],
      startingNextRound: false,
      cancelNextRound: true,
    });
  });
  it('action: DELETE_TOURNAMENT_REQUEST', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.DELETE_TOURNAMENT_REQUEST,
    })).toEqual({
      tournaments: [],
      deletingTournament: true,
    });
  });
  it('action: DELETE_TOURNAMENT_SUCCESS', () => {
    expect(reducer({
      tournaments: [{ _id: 1 }, { _id: 2 }, { _id: 3 }],
    }, {
      type: actionTypes.DELETE_TOURNAMENT_SUCCESS,
      id: 2,
    })).toEqual({
      tournaments: [{ _id: 1 }, { _id: 3 }],
      deletingTournament: false,
    });
  });
  it('action: DELETE_TOURNAMENT_FAILURE', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.DELETE_TOURNAMENT_FAILURE,
      error: new Error('fail err'),
    })).toEqual({
      tournaments: [],
      deletingTournament: false,
      errorDeleteTournament: new Error('fail err'),
    });
  });
  it('action: DELETE_TOURNAMENT_CANCEL', () => {
    expect(reducer({
      tournaments: [],
    }, {
      type: actionTypes.DELETE_TOURNAMENT_CANCEL,
    })).toEqual({
      tournaments: [],
      deletingTournament: false,
    });
  });
});
