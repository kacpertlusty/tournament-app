import actionTypes from '../actionTypes/matchesActionTypes';

const initialState = {
  error: undefined,
  fetched: false,
  fetching: false,
  matches: [],
};

const matchReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.FETCH_MATCHES_REQUEST:
      return {
        ...state,
        fetching: true,
        error: undefined,
        fetched: false,
      };
    case actionTypes.FETCH_MATCHES_SUCCESS:
      return {
        ...state,
        fetching: false,
        matches: action.matches,
        fetched: true,
      };
    case actionTypes.FETCH_MATCHES_FAILURE:
      return {
        ...state,
        fetching: false,
        fetched: true,
        error: action.error,
      };
    case actionTypes.FETCH_MATCHES_CANCEL:
      return {
        ...state,
        fetching: false,
        fetched: false,
      };
    default:
      return { ...state };
  }
};

export default matchReducer;
