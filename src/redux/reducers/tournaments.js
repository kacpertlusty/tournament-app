import actionTypes from '../actionTypes';

const initialState = {
  tournaments: [],
  fetching: false,
  creating: false,
  cancelCreateCreate: false,
  cancelFetch: false,
  failFetch: undefined,
  failCreate: undefined,
  finishedFinishRound: false,
  cancelFinishRound: false,
  finishingRound: false,

  /* NEXT_ROUND */
  startingNextRound: false,
  failNextRound: undefined,
  cancelNextRound: false,
  startedNextRound: false,

  /* DELETE_TOURNAMENT */
  deletingTournament: false,
  errorDeleteTournament: undefined,

  /* JOIN TOURNAMENT */
  joiningTournament: false,
  joinTournamentCancel: false,
  joinTournamentMessage: '',
  errorJoinTournament: undefined,

  /* UNREGISTER FROM TOURNAMENT */
  unregistering: false,
  unregisterCancel: false,
  unregisterMessage: '',
  errorUnregister: undefined,

  /* ADD_SCORE */
  addingScore: false,
  addScoreError: undefined,

  /* UPDATE */
  updating: false,
  updateError: undefined,
};

const tournamentReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    /* FETCH_TOURNAMENT */
    case actionTypes.FETCH_TOURNAMENTS_REQUEST:
      return {
        ...state,
        fetching: true,
        cancelFetch: false,
      };
    case actionTypes.FETCH_TOURNAMENTS_SUCCESS:
      return {
        ...state,
        tournaments: action.tournaments,
        fetching: false,
        cancelFetch: false,
      };
    case actionTypes.FETCH_TOURNAMENTS_FAILURE:
      return { ...state, fetching: false, failFetch: action.fail };
    /* CREATE_TOURNAMENT */
    case actionTypes.CREATE_TOURNAMENT_REQUEST:
      return {
        ...state,
        creating: true,
        cancelCreate: false,
        failCreate: undefined,
      };
    case actionTypes.CREATE_TOURNAMENT_SUCCESS:
      return {
        ...state,
        tournaments: [...state.tournaments, action.tournament],
        creating: false,
        cancelCreate: false,
        failCreate: undefined,
      };
    case actionTypes.CREATE_TOURNAMENT_FAILURE:
      return {
        ...state,
        creating: false,
        failCreate: action.fail,
      };
    case actionTypes.CREATE_TOURNAMENT_CANCEL:
      return {
        ...state,
        creating: false,
        cancelCreate: true,
        failCreate: undefined,
      };
    /* START TOURNAMENT */
    case actionTypes.START_TOURNAMENT_REQUEST:
      return {
        ...state,
        starting: true,
        cancelStart: false,
        failStart: undefined,
        finishedStarting: false,
      };
    case actionTypes.START_TOURNAMENT_SUCCESS:
      return {
        ...state,
        starting: false,
        finishedStarting: true,
        tournaments: state.tournaments.map(
          // eslint-disable-next-line no-underscore-dangle
          t => (t._id === action.tournament._id ? action.tournament : t),
        ),
      };
    case actionTypes.START_TOURNAMENT_FAILURE:
      return {
        ...state,
        starting: false,
        finishedStarting: true,
        failStart: action.fail,
      };
    case actionTypes.START_TOURNAMENT_CANCEL:
      return {
        ...state,
        starting: false,
        cancelStart: false,
      };
    /* FINISH_ROUND_TOURNAMENT */
    case actionTypes.FINISH_ROUND_TOURNAMENT_REQUEST:
      return {
        ...state,
        finishingRound: true,
        cancelFinishRound: false,
        failFinishRound: undefined,
        finishedFinishRound: false,
      };
    case actionTypes.FINISH_ROUND_TOURNAMENT_SUCCESS:
      return {
        ...state,
        finishedFinishRound: true,
        finishingRound: false,
      };
    case actionTypes.FINISH_ROUND_TOURNAMENT_FAILURE:
      return {
        ...state,
        failFinishRound: action.fail,
        finishedFinishRound: true,
        finishingRound: false,
      };
    case actionTypes.FINISH_ROUND_TOURNAMENT_CANCEL:
      return {
        ...state,
        finishingRound: false,
        cancelFinishRound: true,
      };
    /* NEXT_ROUND_TOURNAMENT */
    case actionTypes.NEXT_ROUND_TOURNAMENT_REQUEST:
      return {
        ...state,
        startingNextRound: true,
        failNextRound: undefined,
        cancelNextRound: false,
        startedNextRound: false,
      };
    case actionTypes.NEXT_ROUND_TOURNAMENT_SUCCESS:
      return {
        ...state,
        startingNextRound: false,
        startedNextRound: true,
      };
    case actionTypes.NEXT_ROUND_TOURNAMENT_FAILURE:
      return {
        ...state,
        startingNextRound: false,
        failNextRound: action.fail,
      };
    case actionTypes.NEXT_ROUND_TOURNAMENT_CANCEL:
      return {
        ...state,
        startingNextRound: false,
        cancelNextRound: true,
      };
      /* DELETE_TOURNAMENT */
    case actionTypes.DELETE_TOURNAMENT_REQUEST:
      return {
        ...state,
        deletingTournament: true,
      };
    case actionTypes.DELETE_TOURNAMENT_SUCCESS:
      return {
        ...state,
        // eslint-disable-next-line no-underscore-dangle
        tournaments: state.tournaments.filter(t => t._id !== action.id),
        deletingTournament: false,
      };
    case actionTypes.DELETE_TOURNAMENT_FAILURE:
      return {
        ...state,
        deletingTournament: false,
        errorDeleteTournament: action.error,
      };
    case actionTypes.DELETE_TOURNAMENT_CANCEL:
      return {
        ...state,
        deletingTournament: false,
      };
    /* JOIN TOURNAMENT */
    case actionTypes.JOIN_TOURNAMENT_REQUEST:
      return {
        ...state,
        joiningTournament: true,
        joinTournamentMessage: '',
        errorJoinTournament: undefined,
      };
    case actionTypes.JOIN_TOURNAMENT_SUCCESS:
      return {
        ...state,
        joiningTournament: false,
        tournaments: [
          // eslint-disable-next-line no-underscore-dangle
          ...state.tournaments.filter(t => t._id !== action.tournament._id),
          action.tournament,
        ],
        joinTournamentMessage: action.message,
      };
    case actionTypes.JOIN_TOURNAMENT_FAILURE:
      return {
        ...state,
        joinTournamentMessage: action.message,
        errorJoinTournament: action.error,
        joiningTournament: false,
      };
    case actionTypes.JOIN_TOURNAMENT_CANCEL:
      return {
        ...state,
        joinTournamentMessage: '',
        errorJoinTournament: undefined,
        joiningTournament: false,
      };
    case actionTypes.UNREGISTER_TOURNAMENT_REQUEST:
      return {
        ...state,
        unregistering: true,
        unregisterMessage: '',
        errorUnregister: undefined,
      };
    case actionTypes.UNREGISTER_TOURNAMENT_SUCCESS:
      return {
        ...state,
        tournaments: [
          // eslint-disable-next-line no-underscore-dangle
          ...state.tournaments.filter(t => t._id !== action.tournament._id),
          action.tournament,
        ],
        unregistering: false,
        unregisterMessage: action.message,
      };
    case actionTypes.UNREGISTER_TOURNAMENT_FAILURE:
      return {
        ...state,
        unregistering: false,
        unregisterMessage: action.message,
        errorUnregister: action.error,
      };
    case actionTypes.UNREGISTER_TOURNAMENT_CANCEL:
      return {
        ...state,
        unregisterCancel: true,
        unregistering: false,
      };
    /* ADD_SCORE */
    case actionTypes.ADD_SCORE_REQUEST:
      return {
        ...state,
        addingScore: true,
        addScoreError: undefined,
        addScoreMessage: '',
      };
    case actionTypes.ADD_SCORE_FAILURE:
      return {
        ...state,
        addingScore: false,
        addScoreError: action.error,
      };
    case actionTypes.ADD_SCORE_SUCCESS:
      return {
        ...state,
        addingScore: false,
        addScoreError: undefined,
        tournaments: [
          ...state.tournaments.filter(t => t._id !== action.tournament._id),
          action.tournament,
        ],
      };
    case actionTypes.ADD_SCORE_CANCEL:
      return {
        ...state,
        addingScore: false,
        addScoreError: undefined,
      };
    /* UPDATE */
    case actionTypes.UPDATE_TOURNAMENT_REQUEST:
      return {
        ...state,
        updating: true,
        updateError: undefined,
      };
    case actionTypes.UPDATE_TOURNAMENT_SUCCESS:
      return {
        ...state,
        updating: false,
        updateError: undefined,
        tournaments: [
          ...state.tournaments.filter(t => t._id !== action.tournament._id),
          action.tournament,
        ],
      };
    case actionTypes.UPDATE_TOURNAMENT_FAILURE:
      return {
        ...state,
        updating: false,
        updateError: action.error,
      };
    case actionTypes.UPDATE_TOURNAMENT_CANCEL:
      return {
        ...state,
        updating: false,
        updateError: undefined,
      };
    default:
      return state;
  }
};
export default tournamentReducer;
