import actionTypes from '../actionTypes/player';

const initialState = {
  fetching: false,
  player: {},
  error: undefined,
};

const playerReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.FETCH_PLAYER_REQUEST:
      return {
        ...state,
        fetching: true,
        error: undefined,
      };
    case actionTypes.FETCH_PLAYER_SUCCESS:
      return {
        ...state,
        fetching: false,
        player: action.player,
      };
    case actionTypes.FETCH_PLAYER_FAILURE:
      return {
        fetching: false,
        error: action.error,
        player: {},
      };
    case actionTypes.FETCH_PLAYER_CANCEL:
      return {
        fetching: false,
        error: undefined,
      };
    default:
      return { ...state };
  }
};

export default playerReducer;
