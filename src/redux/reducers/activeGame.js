import actionTypes from '../actionTypes/activeGameActionTypes';

const initialState = {
  fetching: false,
  activeGame: {},
  error: undefined,
  finishing: false,
};

const activeGameReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.FETCH_ACTIVE_GAME_REQUEST:
      return {
        ...state,
        fetching: true,
        error: undefined,
      };
    case actionTypes.FETCH_ACTIVE_GAME_SUCCESS:
      return {
        ...state,
        fetching: false,
        activeGame: action.activeGame,
      };
    case actionTypes.FETCH_ACTIVE_GAME_FAILURE:
      return {
        ...state,
        fetching: false,
        activeGame: {},
        error: action.error,
      };
    case actionTypes.FETCH_ACTIVE_GAME_CANCEL:
      return {
        ...state,
        fetching: false,
        activeGame: {},
        error: undefined,
      };
    case actionTypes.FINISH_ACTIVE_GAME_REQUEST:
      return {
        ...state,
        finishing: true,
        error: undefined,
      };
    case actionTypes.FINISH_ACTIVE_GAME_SUCCESS:
      return {
        ...state,
        finishing: false,
        activeGame: action.activeGame,
      };
    case actionTypes.FINISH_ACTIVE_GAME_FAILURE:
      return {
        ...state,
        finishing: false,
        activeGame: {},
        error: action.error,
      };
    case actionTypes.FINISH_ACTIVE_GAME_CANCEL:
      return {
        ...state,
        finishing: false,
        error: undefined,
      };
    default:
      return {
        ...state,
      };
  }
};

export default activeGameReducer;
