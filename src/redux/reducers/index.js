import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import loginReducer from './login';
import tournamentReducer from './tournaments';
import activeGameReducer from './activeGame';
import matchesReducer from './matches';
import rankingReducer from './ranking';
import playerReducer from './player';

export default history => combineReducers({
  router: connectRouter(history),
  activeGame: activeGameReducer,
  login: loginReducer,
  tournaments: tournamentReducer,
  matches: matchesReducer,
  ranking: rankingReducer,
  player: playerReducer,
});
