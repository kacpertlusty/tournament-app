import login from './login';
import actionTypes from '../actionTypes';

describe('Login reducer test suite', () => {
  const fakeState = { fake_prop: 'fake_value' };
  it('action: LOGIN_REQUEST, should set payload and isLogging', () => {
    expect(login(fakeState, {
      payload: 'fake_payload',
      type: actionTypes.LOGIN_REQUEST,
    })).toEqual({
      fake_prop: 'fake_value',
      isLogging: true,
    });
  });
  it('action: LOGIN_SUCCESS, should set user, logged, isLogging', () => {
    expect(login(fakeState, {
      type: actionTypes.LOGIN_SUCCESS,
      user: 'fake_usr',
      logged: true,
    })).toEqual({
      fake_prop: 'fake_value',
      logged: true,
      isLogging: false,
      user: 'fake_usr',
    });
  });
  it('action: LOGIN_FAILURE', () => {
    expect(login({ ...fakeState, user: 'fake_usr' }, {
      type: actionTypes.LOGIN_FAILURE,
    })).toEqual({
      fake_prop: 'fake_value',
      logged: false,
      user: {},
      isLogging: false,
    });
  });
  it('action: LOGOUT', () => {
    expect(login(fakeState, {
      type: actionTypes.LOGOUT,
    })).toEqual({
      fake_prop: 'fake_value',
      logged: false,
    });
  });
  it('initial state', () => {
    expect(login()).toEqual({
      user: {},
      logged: false,
      isLogging: false,
    });
  });
});
