import actions from '../actionTypes/player';

const fetch = (playerId, token) => ({
  type: actions.FETCH_PLAYER_REQUEST,
  playerId,
  token,
});

const cancel = () => ({
  type: actions.FETCH_PLAYER_CANCEL,
});

export default { cancel, fetch };
