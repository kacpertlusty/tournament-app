import actionTypes from '../actionTypes/ranking';

const fetchRanking = token => ({
  type: actionTypes.FETCH_RANKING_REQUEST,
  token,
});

const cancelFetchRanking = () => ({
  type: actionTypes.FETCH_RANKING_CANCEL,
});

export default { cancelFetchRanking, fetchRanking };
