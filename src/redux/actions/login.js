import actionTypes from '../actionTypes';

function login(email, password) {
  return {
    type: actionTypes.LOGIN_REQUEST,
    payload: { email, password },
  };
}

function logout() {
  return {
    type: actionTypes.LOGOUT,
  };
}

export default { login, logout };
