import actionTypes from '../actionTypes/matchesActionTypes';

const fetchMatches = token => ({
  type: actionTypes.FETCH_MATCHES_REQUEST,
  token,
});

const cancelFetch = () => ({
  type: actionTypes.FETCH_MATCHES_CANCEL,
});

export default { cancelFetch, fetchMatches };
