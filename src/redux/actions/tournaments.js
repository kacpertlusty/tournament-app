import actionTypes from '../actionTypes';

const fetchTournaments = token => ({
  type: actionTypes.FETCH_TOURNAMENTS_REQUEST,
  token,
});

const createTournament = (data, token) => ({
  type: actionTypes.CREATE_TOURNAMENT_REQUEST,
  payload: data,
  token,
});

const cancelCreateTournament = () => ({
  type: actionTypes.CREATE_TOURNAMENT_CANCEL,
});

const startTournament = (id, token) => ({
  type: actionTypes.START_TOURNAMENT_REQUEST,
  id,
  token,
});

const finishRound = (id, token, round) => ({
  type: actionTypes.FINISH_ROUND_TOURNAMENT_REQUEST,
  id,
  token,
  round,
});

const cancelFinishRound = () => ({
  type: actionTypes.FINISH_ROUND_TOURNAMENT_CANCEL,
});

const nextRound = (id, token) => ({
  type: actionTypes.NEXT_ROUND_TOURNAMENT_REQUEST,
  id,
  token,
});

const cancelNextRound = () => ({
  type: actionTypes.NEXT_ROUND_TOURNAMENT_CANCEL,
});

const deleteTournament = (id, token) => ({
  type: actionTypes.DELETE_TOURNAMENT_REQUEST,
  id,
  token,
});

const cancelDeleteTournament = () => ({
  type: actionTypes.DELETE_TOURNAMENT_CANCEL,
});

const joinTournament = (id, token, data) => ({
  type: actionTypes.JOIN_TOURNAMENT_REQUEST,
  data,
  id,
  token,
});

const cancelJoinTournament = () => ({
  type: actionTypes.JOIN_TOURNAMENT_CANCEL,
});

const unregisterFromTournament = (id, token) => ({
  type: actionTypes.UNREGISTER_TOURNAMENT_REQUEST,
  id,
  token,
});

const cancelUnregisterFromTournament = () => ({
  type: actionTypes.UNREGISTER_TOURNAMENT_CANCEL,
});

const addScore = (id, token, playerId, score) => ({
  type: actionTypes.ADD_SCORE_REQUEST,
  id,
  playerId,
  score,
  token,
});

const cancelAddScore = () => ({
  type: actionTypes.ADD_SCORE_CANCEL,
});

const updateTournament = (id, data, token) => {
  const tournament = data;
  delete tournament._id;
  return ({
    type: actionTypes.UPDATE_TOURNAMENT_REQUEST,
    id,
    data: tournament,
    token,
  });
};

const cancelUpdate = () => ({
  type: actionTypes.UPDATE_TOURNAMENT_CANCEL,
});

export default {
  addScore,
  cancelAddScore,
  cancelCreateTournament,
  cancelDeleteTournament,
  cancelFinishRound,
  cancelJoinTournament,
  cancelNextRound,
  cancelUnregisterFromTournament,
  cancelUpdate,
  createTournament,
  deleteTournament,
  fetchTournaments,
  finishRound,
  joinTournament,
  nextRound,
  startTournament,
  unregisterFromTournament,
  updateTournament,
};
