import actionTypes from '../actionTypes/activeGameActionTypes';

const fetchActiveGame = (id, token) => ({
  type: actionTypes.FETCH_ACTIVE_GAME_REQUEST,
  id,
  token,
});

const cancelFetch = () => ({
  type: actionTypes.FETCH_ACTIVE_GAME_CANCEL,
});

const finishGame = (id, token, score) => ({
  type: actionTypes.FINISH_ACTIVE_GAME_REQUEST,
  id,
  token,
  score,
});

const cancelFinish = () => ({
  type: actionTypes.FINISH_ACTIVE_GAME_CANCEL,
});

export default {
  cancelFetch,
  cancelFinish,
  fetchActiveGame,
  finishGame,
};
