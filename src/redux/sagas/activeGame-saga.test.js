import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import sagaHelper from 'redux-saga-testing';
import api from '../../api/activeGame';
import { fetchActiveGame, finishActiveGame } from './activeGame-saga';
import actions from '../actions/activeGame';
import actionTypes from '../actionTypes/activeGameActionTypes';

describe('active game sagas test suite', () => {
  describe('fetch active game saga test suite', () => {
    describe('Scenario 1: successfull fetch data from api', () => {
      const it = sagaHelper(fetchActiveGame(actions.fetchActiveGame('fake id', 'fake token')));
      it('should start race between call to api and dispatching cancel action', (result) => {
        const apiCall = race({
          activeGame: call(api.fetchActiveGame, 'fake id', 'fake token'),
          cancel: take(actionTypes.FETCH_ACTIVE_GAME_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return { activeGame: { match: ['fake', 'obj'] } };
      });
      it('then should put success action on the store with first match from array', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FETCH_ACTIVE_GAME_SUCCESS,
          activeGame: 'fake',
        }));
      });
      it('then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 2: api call interrupted by cancel action', () => {
      const it = sagaHelper(fetchActiveGame(actions.fetchActiveGame('fake id', 'fake token')));
      it('should start race between call to api and dispatching cancel action', (result) => {
        const apiCall = race({
          activeGame: call(api.fetchActiveGame, 'fake id', 'fake token'),
          cancel: take(actionTypes.FETCH_ACTIVE_GAME_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return { cancel: actions.cancelFetch() };
      });
      it('then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 3: api call yields error', () => {
      const it = sagaHelper(fetchActiveGame(actions.fetchActiveGame('fake id', 'fake token')));
      it('should start race between call to api and dispatching cancel action', (result) => {
        const apiCall = race({
          activeGame: call(api.fetchActiveGame, 'fake id', 'fake token'),
          cancel: take(actionTypes.FETCH_ACTIVE_GAME_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return { activeGame: new Error('fake error') };
      });
      it('then should call error to the store', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FETCH_ACTIVE_GAME_FAILURE, error: new Error('fake error'),
        }));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 4: api call returns message with error', () => {
      const it = sagaHelper(fetchActiveGame(actions.fetchActiveGame('fake id', 'fake token')));
      it('should start race between call to api and dispatching cancel action', (result) => {
        const apiCall = race({
          activeGame: call(api.fetchActiveGame, 'fake id', 'fake token'),
          cancel: take(actionTypes.FETCH_ACTIVE_GAME_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return { activeGame: { message: 'fake message' } };
      });
      it('should put message as error to the store', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FETCH_ACTIVE_GAME_FAILURE,
          message: 'fake message',
        }));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
  });
  describe('finish active game saga test suite', () => {
    describe('Scenario 1: successful data fetch from api', () => {
      const it = sagaHelper(finishActiveGame(actions.finishGame('fake id', 'fake token', 'fake score')));
      it('should start race between call to api and dispatching cancel action', (result) => {
        const apiCall = race({
          activeGame: call(api.finishActiveGame, 'fake id', 'fake score', 'fake token'),
          cancel: take(actionTypes.FINISH_ACTIVE_GAME_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return { activeGame: { match: 'fake game' } };
      });
      it('then should put finished game to the store as activeGame', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FINISH_ACTIVE_GAME_SUCCESS,
          activeGame: 'fake game',
        }));
      });
      it('then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 2: api call yields error', () => {
      const it = sagaHelper(finishActiveGame(actions.finishGame('fake id', 'fake token', 'fake score')));
      it('should start race between call to api and dispatching cancel action', (result) => {
        const apiCall = race({
          activeGame: call(api.finishActiveGame, 'fake id', 'fake score', 'fake token'),
          cancel: take(actionTypes.FINISH_ACTIVE_GAME_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return { activeGame: new Error('fake error') };
      });
      it('then should put error to store', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FINISH_ACTIVE_GAME_FAILURE,
          error: new Error('fake error'),
        }));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 3: api returns message as error', () => {
      const it = sagaHelper(finishActiveGame(actions.finishGame('fake id', 'fake token', 'fake score')));
      it('should start race between call to api and dispatching cancel action', (result) => {
        const apiCall = race({
          activeGame: call(api.finishActiveGame, 'fake id', 'fake score', 'fake token'),
          cancel: take(actionTypes.FINISH_ACTIVE_GAME_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return { activeGame: { message: 'fake message' } };
      });
      it('then should put message to the store as error', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FINISH_ACTIVE_GAME_FAILURE,
          error: 'fake message',
        }));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 4: api call interrupted by dispatching cancel action', () => {
      const it = sagaHelper(finishActiveGame(actions.finishGame('fake id', 'fake token', 'fake score')));
      it('should start race between call to api and dispatching cancel action', (result) => {
        const apiCall = race({
          activeGame: call(api.finishActiveGame, 'fake id', 'fake score', 'fake token'),
          cancel: take(actionTypes.FINISH_ACTIVE_GAME_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return { cancel: actions.cancelFinish() };
      });
      it('then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
  });
});
