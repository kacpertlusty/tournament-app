import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import sagaHelper from 'redux-saga-testing';
import api from '../../api/ranking';
import { fetchRanking } from './ranking-saga';
import actions from '../actions/ranking';
import actionTypes from '../actionTypes/ranking';

describe('ranking saga test suite', () => {
  describe('Scenario 1: successfull fetch from api', () => {
    const it = sagaHelper(fetchRanking(actions.fetchRanking('fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        ranking: call(api.fetch, 'fake token'),
        cancel: take(actionTypes.FETCH_RANKING_CANCEL),
      });
      expect(result).toEqual(apiCall);
      return { ranking: ['fake', 'arr'] };
    });
    it('should put new action with actual ranking', (result) => {
      expect(result).toEqual(put({
        type: actionTypes.FETCH_RANKING_SUCCESS,
        players: ['fake', 'arr'],
      }));
    });
    it('and then should do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
  describe('Scenarion 2: api call fails', () => {
    const it = sagaHelper(fetchRanking(actions.fetchRanking('fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        ranking: call(api.fetch, 'fake token'),
        cancel: take(actionTypes.FETCH_RANKING_CANCEL),
      });
      expect(result).toEqual(apiCall);
      return { ranking: new Error('fake error') };
    });
    it('then should put new action with error to store', (result) => {
      expect(result).toEqual(put({
        type: actionTypes.FETCH_RANKING_FAILURE,
        error: new Error('fake error'),
      }));
    });
    it('and then should do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
  describe('Scenario 3: api call is canceled', () => {
    const it = sagaHelper(fetchRanking(actions.fetchRanking('fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        ranking: call(api.fetch, 'fake token'),
        cancel: take(actionTypes.FETCH_RANKING_CANCEL),
      });
      expect(result).toEqual(apiCall);
      return ({ ranking: ['fake arr'], cancel: actions.cancelFetchRanking() });
    });
    it('and then should do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
});
