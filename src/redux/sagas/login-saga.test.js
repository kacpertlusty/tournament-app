import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import { push } from 'connected-react-router';
import sagaHelper from 'redux-saga-testing';
import api from '../../api/user';
import saga from './login-saga';
import actions from '../actions/login';
import actionTypes from '../actionTypes';

describe('login saga test suite', () => {
  describe('Scenario 1: successful login', () => {
    const it = sagaHelper(saga(actions.login('fake@email.com', 'fake_password')));
    it('should start race between api call and logout action take', (result) => {
      const apiCall = race({
        user: call(api.login, { email: 'fake@email.com', password: 'fake_password' }),
        logout: take(actionTypes.LOGOUT),
      });
      expect(result).toEqual(apiCall);
      return { user: { token: 'fake_token' } };
    });
    it('should trigger store action', (result) => {
      expect(result).toEqual(put({
        type: actionTypes.LOGIN_SUCCESS,
        user: 'fake_token',
        logged: true,
      }));
    });
    it('should push new route to router', (result) => {
      expect(result).toEqual(put(push('/showTournaments')));
    });
    it('and then do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
  describe('Scenario 2: fail login', () => {
    const it = sagaHelper(saga(actions.login('fake email', 'fake pass')));
    it('should start race between api call and logout action take', (result) => {
      const apiCall = race({
        user: call(api.login, { email: 'fake email', password: 'fake pass' }),
        logout: take(actionTypes.LOGOUT),
      });
      expect(result).toEqual(apiCall);
      return { user: { errors: { 'email or password': 'is invalid' } } };
    });
    it('should trigger fail login action', (result) => {
      expect(result).toEqual(put({ type: actionTypes.LOGIN_FAILURE, logged: false }));
    });
    it('and then do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
  describe('Scenario 3: missing email or password', () => {
    const it = sagaHelper(saga(actions.login('')));
    it('should start race between api call and logout action take', (result) => {
      const apiCall = race({
        user: call(api.login, { email: '', password: undefined }),
        logout: take(actionTypes.LOGOUT),
      });
      expect(result).toEqual(apiCall);
      return { user: { message: 'Missing credentials' } };
    });
    it('should trigger fail login action', (result) => {
      expect(result).toEqual(put({ type: actionTypes.LOGIN_FAILURE, logged: false }));
    });
    it('and then do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
});
