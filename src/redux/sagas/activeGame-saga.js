import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import { activeGame } from '../../api';
import actionTypes from '../actionTypes/activeGameActionTypes';

export function* fetchActiveGame(action) {
  const { id, token } = action;
  const winner = yield race({
    activeGame: call(activeGame.fetchActiveGame, id, token),
    cancel: take(actionTypes.FETCH_ACTIVE_GAME_CANCEL),
  });
  if (winner.activeGame instanceof Error) {
    yield put({ type: actionTypes.FETCH_ACTIVE_GAME_FAILURE, error: winner.activeGame });
  } else if (winner.activeGame && winner.activeGame.message) {
    yield put({ type: actionTypes.FETCH_ACTIVE_GAME_FAILURE, message: winner.activeGame.message });
  } else if (!winner.cancel && winner.activeGame.match) {
    const { match } = winner.activeGame;
    yield put({
      type: actionTypes.FETCH_ACTIVE_GAME_SUCCESS,
      activeGame: match.length > 0 ? match[0] : {},
    });
  }
}

export function* finishActiveGame(action) {
  const { id, score, token } = action;
  const winner = yield race({
    activeGame: call(activeGame.finishActiveGame, id, score, token),
    cancel: take(actionTypes.FINISH_ACTIVE_GAME_CANCEL),
  });
  if (winner.activeGame instanceof Error) {
    yield put({ type: actionTypes.FINISH_ACTIVE_GAME_FAILURE, error: winner.activeGame });
  } else if (winner.activeGame && winner.activeGame.message) {
    yield put({ type: actionTypes.FINISH_ACTIVE_GAME_FAILURE, error: winner.activeGame.message });
  } else if (!winner.cancel && winner.activeGame.match) {
    yield put({
      type: actionTypes.FINISH_ACTIVE_GAME_SUCCESS,
      activeGame: winner.activeGame.match,
    });
  }
}

export default { fetchActiveGame, finishActiveGame };
