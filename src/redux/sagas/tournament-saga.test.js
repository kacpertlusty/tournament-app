import sagaHelper from 'redux-saga-testing';
import {
  call,
  put,
  race,
  take,
} from 'redux-saga/effects';
import { push } from 'connected-react-router';
import api from '../../api/tournament';
import {
  fetchTournamentsAsync,
  createTournamentAsync,
  startTournamentAsync,
  finishRoundAsync,
  nextRoundAsync,
  deleteTournamentAsync,
} from './tournament-saga';
import actions from '../actions/tournaments';
import actionTypes from '../actionTypes';

describe('tournament sagas test suite', () => {
  describe('fetch tournaments saga', () => {
    describe('Scenario 1: successfull data fetch', () => {
      const it = sagaHelper(fetchTournamentsAsync(actions.fetchTournaments('fake_token')));

      it('should start race and call api, api resolves with tournaments', (result) => {
        const apiCall = race({
          tournaments: call(api.fetchAll, { token: 'fake_token' }),
          cancelation: take(actionTypes.LOGOUT),
        });
        expect(result).toEqual(apiCall);

        return ({ tournaments: [] });
      });
      it('should trigger action with fetched tournaments', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FETCH_TOURNAMENTS_SUCCESS,
          tournaments: [],
        }));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 2: fail data fetch', () => {
      const it = sagaHelper(fetchTournamentsAsync(actions.fetchTournaments('fake_token')));
      it('should start race and call api, api resolves error', (result) => {
        const apiCall = race({
          tournaments: call(api.fetchAll, { token: 'fake_token' }),
          cancelation: take(actionTypes.LOGOUT),
        });
        expect(result).toEqual(apiCall);
        return ({ tournaments: new Error('fake err') });
      });
      it('should trigger failure action with error', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FETCH_TOURNAMENTS_FAILURE,
          fail: new Error('fake err'),
        }));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
  });
  describe('create tournament saga', () => {
    describe('Scenario 1: tournament is created successfully', () => {
      const it = sagaHelper(createTournamentAsync(actions.createTournament({
        name: 'fake name',
        args: 'fake args',
      }, 'fake_token')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.create, {
            name: 'fake name',
            args: 'fake args',
          }, 'fake_token'),
          cancelation: take(actionTypes.CREATE_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: { name: 'fake tournament', _id: 'fake id' } });
      });
      it('should trigger success action', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.CREATE_TOURNAMENT_SUCCESS, tournament: { name: 'fake tournament', _id: 'fake id' },
        }));
      });
      it('should push route change', (result) => {
        expect(result).toEqual(put(push('/tournamentDetails', 'fake id')));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 2: tournament api call returns error', () => {
      const it = sagaHelper(createTournamentAsync(actions.createTournament({}, 'fake_token')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.create, {}, 'fake_token'),
          cancelation: take(actionTypes.CREATE_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: new Error('fake error') });
      });
      it('should trigger failure action', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.CREATE_TOURNAMENT_FAILURE, fail: new Error('fake error'),
        }));
      });
      it('and then do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 3: cancelation wins race', () => {
      const it = sagaHelper(createTournamentAsync(actions.createTournament({}, '')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.create, {}, ''),
          cancelation: take(actionTypes.CREATE_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ cancelation: { type: actionTypes.CREATE_TOURNAMENT_CANCEL } });
      });
      it('do nothing when cancelation wins race', (result) => {
        expect(result).toBeUndefined();
      });
    });
  });
  describe('start tournament saga', () => {
    describe('Scenario 1: tournament is started successfully', () => {
      const it = sagaHelper(startTournamentAsync(actions.startTournament({ name: 'fake_id' }, 'fake_token')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.start, { name: 'fake_id' }, 'fake_token'),
          cancelation: take(actionTypes.START_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: ['round1', 'round2'] });
      });
      it('when tournament is started succesfully, trigger success action', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.START_TOURNAMENT_SUCCESS,
          tournament: ['round1', 'round2'],
        }));
      });
      it('should initiate fetching tournaments', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FETCH_TOURNAMENTS_REQUEST,
          token: 'fake_token',
        }));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 2: tournament failed', () => {
      const it = sagaHelper(startTournamentAsync(actions.startTournament('fake obj', 'token_id')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.start, 'fake obj', 'token_id'),
          cancelation: take(actionTypes.START_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: new Error('fake err') });
      });
      it('should trigger fail action on store', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.START_TOURNAMENT_FAILURE,
          fail: new Error('fake err'),
        }));
      });
      it('and then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 3: cancelation', () => {
      const it = sagaHelper(startTournamentAsync(actions.startTournament('', '')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.start, '', ''),
          cancelation: take(actionTypes.START_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ cancelation: { type: actionTypes.START_TOURNAMENT_CANCEL } });
      });
      it('and do nothing when task was cancelled', (result) => {
        expect(result).toBeUndefined();
      });
    });
  });
  describe('finish round saga', () => {
    describe('Scenario 1: round is finished successfully', () => {
      const it = sagaHelper(finishRoundAsync(actions.finishRound('fake_id', 'fake_token', 1)));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.finishRound, 'fake_id', 'fake_token', 1),
          cancelation: take(actionTypes.FINISH_ROUND_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: { name: 'fake_name' } });
      });
      it('should trigger success action', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FINISH_ROUND_TOURNAMENT_SUCCESS,
          tournament: { name: 'fake_name' },
        }));
      });
      it('should fetch tournaments', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FETCH_TOURNAMENTS_REQUEST,
          token: 'fake_token',
        }));
      });
      it('and then do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 2: api call resolves with error', () => {
      const it = sagaHelper(finishRoundAsync(actions.finishRound('', '', '')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.finishRound, '', '', ''),
          cancelation: take(actionTypes.FINISH_ROUND_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: new Error('fake err') });
      });
      it('should trigger failure action', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FINISH_ROUND_TOURNAMENT_FAILURE,
          fail: new Error('fake err'),
        }));
      });
      it('and then do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 3: cancelation', () => {
      const it = sagaHelper(finishRoundAsync(actions.finishRound('', '', '')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.finishRound, '', '', ''),
          cancelation: take(actionTypes.FINISH_ROUND_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ cancelation: { type: actionTypes.FINISH_ROUND_TOURNAMENT_CANCEL } });
      });
      it('and then do nothing when task was cancelled', (result) => {
        expect(result).toBeUndefined();
      });
    });
  });
  describe('next round saga', () => {
    describe('Scenario 1: next round is started successfully', () => {
      const it = sagaHelper(nextRoundAsync(actions.nextRound('fake_id', 'fake_token')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.nextRound, 'fake_id', 'fake_token'),
          cancelation: take(actionTypes.NEXT_ROUND_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: { name: 'fake name' } });
      });
      it('should trigger success action to store', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.NEXT_ROUND_TOURNAMENT_SUCCESS,
          tournament: { name: 'fake name' },
        }));
      });
      it('should initiate fetching tournaments', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.FETCH_TOURNAMENTS_REQUEST,
          token: 'fake_token',
        }));
      });
      it('and then do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 2: api call resolves with error', () => {
      const it = sagaHelper(nextRoundAsync(actions.nextRound('fake_id', 'fake_token')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.nextRound, 'fake_id', 'fake_token'),
          cancelation: take(actionTypes.NEXT_ROUND_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: new Error('fake err') });
      });
      it('should trigger error action', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.NEXT_ROUND_TOURNAMENT_FAILURE,
          fail: new Error('fake err'),
        }));
      });
      it('and then do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 3: cancelation', () => {
      const it = sagaHelper(nextRoundAsync(actions.nextRound('fake_id', 'fake_token')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.nextRound, 'fake_id', 'fake_token'),
          cancelation: take(actionTypes.NEXT_ROUND_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ cancelation: { type: actionTypes.NEXT_ROUND_TOURNAMENT_CANCEL } });
      });
      it('and then do nothing, because task was cancelled', (result) => {
        expect(result).toBeUndefined();
      });
    });
  });
  describe('delete tournament saga', () => {
    describe('Scenario 1: tournament successfully deleted', () => {
      const it = sagaHelper(deleteTournamentAsync(actions.deleteTournament('fake_id', 'fake_token')));
      it('should start race between api call and cancelation task', (result) => {
        const apiCall = race({
          tournament: call(api.deleteTournament, 'fake_id', 'fake_token'),
          cancelation: take(actionTypes.DELETE_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: 'fake msg' });
      });
      it('then should yield success', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.DELETE_TOURNAMENT_SUCCESS,
          id: 'fake_id',
        }));
      });
      it('then should change current route', (result) => {
        expect(result).toEqual(put(push('/showTournaments')));
      });
      it('then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 2: tournament api call ends with error', () => {
      const it = sagaHelper(deleteTournamentAsync(actions.deleteTournament('fake_id', '')));
      const fakeErr = new Error('fake error');
      it('should start race between api call and cancelation', (result) => {
        const apiCall = race({
          tournament: call(api.deleteTournament, 'fake_id', ''),
          cancelation: take(actionTypes.DELETE_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ tournament: fakeErr });
      });
      it('then should yield failure', (result) => {
        expect(result).toEqual(put({
          type: actionTypes.DELETE_TOURNAMENT_FAILURE,
          error: fakeErr,
        }));
      });
      it('then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
    describe('Scenario 3: cancelation', () => {
      const it = sagaHelper(deleteTournamentAsync(actions.deleteTournament('', '')));
      it('should start race between api call and cancelation', (result) => {
        const apiCall = race({
          tournament: call(api.deleteTournament, '', ''),
          cancelation: take(actionTypes.DELETE_TOURNAMENT_CANCEL),
        });
        expect(result).toEqual(apiCall);
        return ({ cancelation: {} });
      });
      it('then should do nothing', (result) => {
        expect(result).toBeUndefined();
      });
    });
  });
});
