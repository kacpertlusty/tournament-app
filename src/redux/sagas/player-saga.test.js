import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import sagaHelper from 'redux-saga-testing';
import api from '../../api/player';
import { fetchPlayer } from './player-saga';
import actions from '../actions/player';
import actionTypes from '../actionTypes/player';

describe('player saga test suite', () => {
  describe('Scenario 1: successful player fetch from api by his id', () => {
    const it = sagaHelper(fetchPlayer(actions.fetch('fake id', 'fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        player: call(api.fetchPlayer, 'fake id', 'fake token'),
        cancel: take(actionTypes.FETCH_PLAYER_CANCEL),
      });
      expect(result).toEqual(apiCall);
      return { player: 'fake obj' };
    });
    it('then should put action to the store with given player', (result) => {
      expect(result).toEqual(put({
        type: actionTypes.FETCH_PLAYER_SUCCESS,
        player: 'fake obj',
      }));
    });
    it('and then should do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
  describe('Scenario 2: api call yields error', () => {
    const it = sagaHelper(fetchPlayer(actions.fetch('fake id', 'fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        player: call(api.fetchPlayer, 'fake id', 'fake token'),
        cancel: take(actionTypes.FETCH_PLAYER_CANCEL),
      });
      expect(result).toEqual(apiCall);
      return { player: new Error('fake error') };
    });
    it('then should put action to the store with given player', (result) => {
      expect(result).toEqual(put({
        type: actionTypes.FETCH_PLAYER_FAILURE,
        error: new Error('fake error'),
      }));
    });
    it('and then should do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
  describe('Scenario 3: api call interrupted by cancel action', () => {
    const it = sagaHelper(fetchPlayer(actions.fetch('fake id', 'fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        player: call(api.fetchPlayer, 'fake id', 'fake token'),
        cancel: take(actionTypes.FETCH_PLAYER_CANCEL),
      });
      expect(result).toEqual(apiCall);
      return { cancel: actions.cancel() };
    });
    it('and then should do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
});
