import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { user } from '../../api';
import actionTypes from '../actionTypes';

export default function* loginAsync(action) {
  const { email, password } = action.payload;
  const winner = yield race({
    user: call(user.login, { email, password }),
    logout: take(actionTypes.LOGOUT),
  });

  if (winner.user.token) {
    yield put({
      type: actionTypes.LOGIN_SUCCESS,
      user: winner.user.token,
      logged: true,
    });
    yield put(push('/showTournaments')); // Go to dashboard page
  } else if (!winner.logout) {
    yield put({ type: actionTypes.LOGIN_FAILURE, logged: false });
  }
}
