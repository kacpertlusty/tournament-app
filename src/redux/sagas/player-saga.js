import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import actionTypes from '../actionTypes/player';
import api from '../../api/player';

export function* fetchPlayer(action) {
  const { playerId, token } = action;
  const winner = yield race({
    player: call(api.fetchPlayer, playerId, token),
    cancel: take(actionTypes.FETCH_PLAYER_CANCEL),
  });
  if (winner.player instanceof Error) {
    yield put({ type: actionTypes.FETCH_PLAYER_FAILURE, error: winner.player });
  } else if (!winner.cancel) {
    yield put({ type: actionTypes.FETCH_PLAYER_SUCCESS, player: winner.player });
  }
}

export default { fetchPlayer };
