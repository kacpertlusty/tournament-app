import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import { matches } from '../../api';
import actionTypes from '../actionTypes/matchesActionTypes';

export function* fetchMatches(action) {
  const { token } = action;
  const winner = yield race({
    matches: call(matches.fetchMatches, token),
    cancel: take(actionTypes.FETCH_MATCHES_CANCEL),
  });
  if (winner.matches instanceof Error) {
    yield put({ type: actionTypes.FETCH_MATCHES_FAILURE, error: winner.matches });
  } else if (!winner.cancel) {
    yield put({ type: actionTypes.FETCH_MATCHES_SUCCESS, matches: winner.matches.match });
  }
}

export default { fetchMatches };
