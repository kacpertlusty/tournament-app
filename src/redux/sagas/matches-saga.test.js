import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import sagaHelper from 'redux-saga-testing';
import api from '../../api/matches';
import { fetchMatches } from './matches-saga';
import actions from '../actions/matches';
import actionTypes from '../actionTypes/matchesActionTypes';

describe('matches saga test suite', () => {
  describe('Scenario 1: successful data fetch from api', () => {
    const it = sagaHelper(fetchMatches(actions.fetchMatches('fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        matches: call(api.fetchMatches, 'fake token'),
        cancel: take(actionTypes.FETCH_MATCHES_CANCEL),
      });
      expect(result).toEqual(apiCall);
      return { matches: { match: ['1', '2'] } };
    });
    it('then should put matches to the store', (result) => {
      expect(result).toEqual(put({
        type: actionTypes.FETCH_MATCHES_SUCCESS,
        matches: ['1', '2'],
      }));
    });
    it('and then should do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
  describe('Scenario 2: api call yields error', () => {
    const it = sagaHelper(fetchMatches(actions.fetchMatches('fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        matches: call(api.fetchMatches, 'fake token'),
        cancel: take(actionTypes.FETCH_MATCHES_CANCEL),
      });
      expect(apiCall).toEqual(result);
      return { matches: new Error('fake error') };
    });
    it('then should put error to the store', (result) => {
      expect(result).toEqual(put({
        type: actionTypes.FETCH_MATCHES_FAILURE,
        error: new Error('fake error'),
      }));
    });
  });
  describe('Scenario 3: api call interrupted by dispatched cancel action', () => {
    const it = sagaHelper(fetchMatches(actions.fetchMatches('fake token')));
    it('should start race between api call and cancel action', (result) => {
      const apiCall = race({
        matches: call(api.fetchMatches, 'fake token'),
        cancel: take(actionTypes.FETCH_MATCHES_CANCEL),
      });
      expect(apiCall).toEqual(result);
      return { cancel: actions.cancelFetch() };
    });
    it('and then should do nothing', (result) => {
      expect(result).toBeUndefined();
    });
  });
});
