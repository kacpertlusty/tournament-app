import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import { ranking } from '../../api';
import actionTypes from '../actionTypes/ranking';

export function* fetchRanking(action) {
  const { token } = action;
  const winner = yield race({
    ranking: call(ranking.fetch, token),
    cancel: take(actionTypes.FETCH_RANKING_CANCEL),
  });
  if (winner.ranking instanceof Error) {
    yield put({ type: actionTypes.FETCH_RANKING_FAILURE, error: winner.ranking });
  } else if (!winner.cancel) {
    yield put({ type: actionTypes.FETCH_RANKING_SUCCESS, players: winner.ranking });
  }
}

export default { fetchRanking };
