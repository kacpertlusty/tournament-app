import { takeEvery, all } from 'redux-saga/effects';
import {
  addScore,
  fetchTournamentsAsync,
  createTournamentAsync,
  startTournamentAsync,
  finishRoundAsync,
  nextRoundAsync,
  deleteTournamentAsync,
  joinTournamentAsync,
  unregisterFromTournament,
  update,
} from './tournament-saga';
import actionTypes from '../actionTypes';
import loginSaga from './login-saga';
import { fetchActiveGame, finishActiveGame } from './activeGame-saga';
import { fetchMatches } from './matches-saga';
import { fetchRanking } from './ranking-saga';
import { fetchPlayer } from './player-saga';

export default function* rootSaga() {
  yield all([
    /* LOGIN SAGA */
    takeEvery(actionTypes.LOGIN_REQUEST, loginSaga),
    /* TOURNAMENT SAGAS */
    takeEvery(actionTypes.FETCH_TOURNAMENTS_REQUEST, fetchTournamentsAsync),
    takeEvery(actionTypes.CREATE_TOURNAMENT_REQUEST, createTournamentAsync),
    takeEvery(actionTypes.START_TOURNAMENT_REQUEST, startTournamentAsync),
    takeEvery(actionTypes.FINISH_ROUND_TOURNAMENT_REQUEST, finishRoundAsync),
    takeEvery(actionTypes.NEXT_ROUND_TOURNAMENT_REQUEST, nextRoundAsync),
    takeEvery(actionTypes.DELETE_TOURNAMENT_REQUEST, deleteTournamentAsync),
    takeEvery(actionTypes.JOIN_TOURNAMENT_REQUEST, joinTournamentAsync),
    takeEvery(actionTypes.UNREGISTER_TOURNAMENT_REQUEST, unregisterFromTournament),
    takeEvery(actionTypes.ADD_SCORE_REQUEST, addScore),
    takeEvery(actionTypes.UPDATE_TOURNAMENT_REQUEST, update),
    /* ACTIVE GAME SAGA */
    takeEvery(actionTypes.FETCH_ACTIVE_GAME_REQUEST, fetchActiveGame),
    takeEvery(actionTypes.FINISH_ACTIVE_GAME_REQUEST, finishActiveGame),
    /* MATCHES SAGA */
    takeEvery(actionTypes.FETCH_MATCHES_REQUEST, fetchMatches),
    /* RANKING SAGA */
    takeEvery(actionTypes.FETCH_RANKING_REQUEST, fetchRanking),
    /* PLAYER SAGA */
    takeEvery(actionTypes.FETCH_PLAYER_REQUEST, fetchPlayer),
  ]);
}
