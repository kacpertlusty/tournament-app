import {
  take,
  put,
  call,
  race,
} from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { tournament } from '../../api';
import actionTypes from '../actionTypes';

export function* fetchTournamentsAsync(action) {
  const { token } = action;
  const winner = yield race({
    tournaments: call(tournament.fetchAll, { token }),
    cancelation: take(actionTypes.LOGOUT),
  });

  if (!(winner.tournaments instanceof Error)) {
    yield put({ type: actionTypes.FETCH_TOURNAMENTS_SUCCESS, tournaments: winner.tournaments });
  } else {
    yield put({ type: actionTypes.FETCH_TOURNAMENTS_FAILURE, fail: winner.tournaments });
  }
}

export function* createTournamentAsync(action) {
  const { payload, token } = action;
  const winner = yield race({
    tournament: call(tournament.create, payload, token),
    cancelation: take(actionTypes.CREATE_TOURNAMENT_CANCEL),
  });

  if (winner.tournament instanceof Error) {
    yield put({ type: actionTypes.CREATE_TOURNAMENT_FAILURE, fail: winner.tournament });
  } else if (!winner.cancelation) {
    yield put({ type: actionTypes.CREATE_TOURNAMENT_SUCCESS, tournament: winner.tournament });
    yield put(push('/tournamentDetails', winner.tournament._id));
  }
}

export function* startTournamentAsync(action) {
  const { id, token } = action;
  const winner = yield race({
    tournament: call(tournament.start, id, token),
    cancelation: take(actionTypes.START_TOURNAMENT_CANCEL),
  });
  if (winner.tournament instanceof Error) {
    yield put({ type: actionTypes.START_TOURNAMENT_FAILURE, fail: winner.tournament });
  } else if (!winner.cancelation) {
    yield put({ type: actionTypes.START_TOURNAMENT_SUCCESS, tournament: winner.tournament });
    yield put({ type: actionTypes.FETCH_TOURNAMENTS_REQUEST, token });
  }
}

export function* finishRoundAsync(action) {
  const { id, token, round } = action;
  const winner = yield race({
    tournament: call(tournament.finishRound, id, token, round),
    cancelation: take(actionTypes.FINISH_ROUND_TOURNAMENT_CANCEL),
  });

  if (winner.tournament instanceof Error) {
    yield put({ type: actionTypes.FINISH_ROUND_TOURNAMENT_FAILURE, fail: winner.tournament });
  } else if (!winner.cancelation) {
    yield put({ type: actionTypes.FINISH_ROUND_TOURNAMENT_SUCCESS, tournament: winner.tournament });
    yield put({ type: actionTypes.FETCH_TOURNAMENTS_REQUEST, token });
  }
}

export function* nextRoundAsync(action) {
  const { id, token } = action;
  const winner = yield race({
    tournament: call(tournament.nextRound, id, token),
    cancelation: take(actionTypes.NEXT_ROUND_TOURNAMENT_CANCEL),
  });

  if (winner.tournament instanceof Error) {
    yield put({ type: actionTypes.NEXT_ROUND_TOURNAMENT_FAILURE, fail: winner.tournament });
  } else if (!winner.cancelation) {
    yield put({ type: actionTypes.NEXT_ROUND_TOURNAMENT_SUCCESS, tournament: winner.tournament });
    yield put({ type: actionTypes.FETCH_TOURNAMENTS_REQUEST, token });
  }
}

export function* deleteTournamentAsync(action) {
  const { id, token } = action;
  const winner = yield race({
    tournament: call(tournament.deleteTournament, id, token),
    cancelation: take(actionTypes.DELETE_TOURNAMENT_CANCEL),
  });
  if (winner.tournament instanceof Error) {
    yield put({ type: actionTypes.DELETE_TOURNAMENT_FAILURE, error: winner.tournament });
  } else if (!winner.cancelation) {
    yield put({ type: actionTypes.DELETE_TOURNAMENT_SUCCESS, id });
    yield put(push('/showTournaments'));
  }
}

export function* joinTournamentAsync(action) {
  const { data, id, token } = action;
  const winner = yield race({
    join: call(tournament.joinTournament, id, token, data),
    cancel: take(actionTypes.JOIN_TOURNAMENT_CANCEL),
  });
  if (winner.join instanceof Error) {
    yield put({ type: actionTypes.JOIN_TOURNAMENT_FAILURE, error: winner.join });
  } else if (winner.join.message !== 'Dodano do turnieju') {
    yield put({ type: actionTypes.JOIN_TOURNAMENT_FAILURE, message: winner.join.message });
  } else if (!winner.cancelation) {
    const updatedTournament = yield call(tournament.fetchSingleTournament, id, token);
    yield put({
      type: actionTypes.JOIN_TOURNAMENT_SUCCESS,
      tournament: updatedTournament[0],
      message: winner.join,
    });
    // eslint-disable-next-line no-underscore-dangle
    yield put(push('/tournamentDetails', updatedTournament[0]._id));
  }
}

export function* unregisterFromTournament(action) {
  const { id, token } = action;
  const winner = yield race({
    unregister: call(tournament.unregisterFromTournament, id, token),
    cancel: take(actionTypes.UNREGISTER_TOURNAMENT_CANCEL),
  });
  if (winner.join instanceof Error) {
    yield put({ type: actionTypes.UNREGISTER_TOURNAMENT_FAILURE, error: winner.unregister });
  } else if (winner.unregister.message !== 'user have been unregistered from tournament') {
    yield put({ type: actionTypes.UNREGISTER_TOURNAMENT_FAILURE, message: winner.join.message });
  } else if (!winner.cancelation) {
    const updatedTournament = yield call(tournament.fetchSingleTournament, id, token);
    yield put({
      type: actionTypes.UNREGISTER_TOURNAMENT_SUCCESS,
      tournament: updatedTournament[0],
      message: winner.join,
    });
  }
}

export function* addScore(action) {
  const {
    id,
    playerId,
    score,
    token,
  } = action;
  const winner = yield race({
    addScore: call(tournament.addScore, id, playerId, score, token),
    cancellation: take(actionTypes.ADD_SCORE_CANCEL),
  });
  if (winner.addScore instanceof Error) {
    yield put({ type: actionTypes.ADD_SCORE_FAILURE, error: winner.addScore });
  } else if (!winner.cancellation) {
    yield put({ type: actionTypes.ADD_SCORE_SUCCESS, tournament: winner.addScore });
  }
}

export function* update(action) {
  const {
    id,
    data,
    token,
  } = action;
  const winner = yield race({
    update: call(tournament.update, id, data, token),
    cancellation: take(actionTypes.UPDATE_TOURNAMENT_CANCEL),
  });
  if (winner.update instanceof Error) {
    yield put({ type: actionTypes.UPDATE_TOURNAMENT_FAILURE, error: winner.update });
  } else if (!winner.cancellation) {
    yield put({ type: actionTypes.UPDATE_TOURNAMENT_SUCCESS, tournament: winner.update });
  }
}

export default {
  createTournamentAsync,
  deleteTournamentAsync,
  fetchTournamentsAsync,
  finishRoundAsync,
  joinTournamentAsync,
  nextRoundAsync,
  startTournamentAsync,
  unregisterFromTournament,
  update,
};
