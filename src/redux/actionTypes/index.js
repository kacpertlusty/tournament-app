import activeGameActionTypes from './activeGameActionTypes';
import matchesActionTypes from './matchesActionTypes';
import rankingActionTypes from './ranking';
import playerActionTypes from './player';

const actionTypes = {
  ...activeGameActionTypes,
  ...matchesActionTypes,
  ...rankingActionTypes,
  ...playerActionTypes,

  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_FAILURE: 'LOGIN_FAILURE',

  LOGOUT: 'LOGOUT',

  FETCH_TOURNAMENTS_REQUEST: 'FETCH_TOURNAMENTS_REQUEST',
  FETCH_TOURNAMENTS_SUCCESS: 'FETCH_TOURNAMENTS_SUCCESS',
  FETCH_TOURNAMENTS_FAILURE: 'FETCH_TOURNAMENTS_FAILURE',

  CREATE_TOURNAMENT_REQUEST: 'CREATE_TOURNAMENT_REQUEST',
  CREATE_TOURNAMENT_SUCCESS: 'CREATE_TOURNAMENT_SUCCESS',
  CREATE_TOURNAMENT_FAILURE: 'CREATE_TOURNAMENT_FAILURE',
  CREATE_TOURNAMENT_CANCEL: 'CREATE_TOURNAMENT_CANCEL',

  START_TOURNAMENT_REQUEST: 'START_TOURNAMENT_REQUEST',
  START_TOURNAMENT_SUCCESS: 'START_TOURNAMENT_SUCCESS',
  START_TOURNAMENT_FAILURE: 'START_TOURNAMENT_FAILURE',
  START_TOURNAMENT_CANCEL: 'START_TOURNAMENT_CANCEL',

  FINISH_ROUND_TOURNAMENT_REQUEST: 'FINISH_ROUND_TOURNAMENT_REQUEST',
  FINISH_ROUND_TOURNAMENT_SUCCESS: 'FINISH_ROUND_TOURNAMENT_SUCCESS',
  FINISH_ROUND_TOURNAMENT_FAILURE: 'FINISH_ROUND_TOURNAMENT_FAILURE',
  FINISH_ROUND_TOURNAMENT_CANCEL: 'FINISH_ROUND_TOURNAMENT_CANCEL',

  NEXT_ROUND_TOURNAMENT_REQUEST: 'NEXT_ROUND_TOURNAMENT_REQUEST',
  NEXT_ROUND_TOURNAMENT_SUCCESS: 'NEXT_ROUND_TOURNAMENT_SUCCESS',
  NEXT_ROUND_TOURNAMENT_FAILURE: 'NEXT_ROUND_TOURNAMENT_FAILURE',
  NEXT_ROUND_TOURNAMENT_CANCEL: 'NEXT_ROUND_TOURNAMENT_CANCEL',

  DELETE_TOURNAMENT_REQUEST: 'DELETE_TOURNAMENT_REQUEST',
  DELETE_TOURNAMENT_SUCCESS: 'DELETE_TOURNAMENT_SUCCESS',
  DELETE_TOURNAMENT_FAILURE: 'DELETE_TOURNAMENT_FAILURE',
  DELETE_TOURNAMENT_CANCEL: 'DELETE_TOURNAMENT_CANCEL',

  JOIN_TOURNAMENT_REQUEST: 'JOIN_TOURNAMENT_REQUEST',
  JOIN_TOURNAMENT_SUCCESS: 'JOIN_TOURNAMENT_SUCCESS',
  JOIN_TOURNAMENT_FAILURE: 'JOIN_TOURNAMENT_FAILURE',
  JOIN_TOURNAMENT_CANCEL: 'JOIN_TOURNAMENT_CANCEL',

  UNREGISTER_TOURNAMENT_REQUEST: 'UNREGISTER_TOURNAMENT_REQUEST',
  UNREGISTER_TOURNAMENT_SUCCESS: 'UNREGISTER_TOURNAMENT_SUCCESS',
  UNREGISTER_TOURNAMENT_FAILURE: 'UNREGISTER_TOURNAMENT_FAILURE',
  UNREGISTER_TOURNAMENT_CANCEL: 'UNREGISTER_TOURNAMENT_CANCEL',

  ADD_SCORE_REQUEST: 'ADD_SCORE_REQUEST',
  ADD_SCORE_SUCCESS: 'ADD_SCORE_SUCCESS',
  ADD_SCORE_FAILURE: 'ADD_SCORE_FAILURE',
  ADD_SCORE_CANCEL: 'ADD_SCORE_CANCEL',

  UPDATE_TOURNAMENT_REQUEST: 'UPDATE_TOURNAMENT_REQUEST',
  UPDATE_TOURNAMENT_SUCCESS: 'UPDATE_TOURNAMENT_SUCCESS',
  UPDATE_TOURNAMENT_FAILURE: 'UPDATE_TOURNAMENT_FAILURE',
  UPDATE_TOURNAMENT_CANCEL: 'UPDATE_TOURNAMENT_CANCEL',
};

export default actionTypes;
