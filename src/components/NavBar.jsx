import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { LinkContainer } from 'react-router-bootstrap';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import userActions from '../redux/actions/login';

const getLoggedContent = (logged, user, dispatch) => {
  if (!logged) {
    return (
      <LinkContainer to="/login">
        <Nav.Link>Zaloguj</Nav.Link>
      </LinkContainer>
    );
  }
  return (
    <>
      <Nav.Link>{`Witaj ${user.firstName} ${user.lastName}`}</Nav.Link>
      <LinkContainer to="/showTournaments">
        <Nav.Link>Wyświetl turnieje</Nav.Link>
      </LinkContainer>
      <LinkContainer to="/createTournament">
        <Nav.Link>Utwórz turniej</Nav.Link>
      </LinkContainer>
      <LinkContainer to="/activeGame">
        <Nav.Link>Aktywny mecz</Nav.Link>
      </LinkContainer>
      <LinkContainer to="/showMatches">
        <Nav.Link>Historia gier</Nav.Link>
      </LinkContainer>
      <LinkContainer to="/playerRanking">
        <Nav.Link>Ranking graczy</Nav.Link>
      </LinkContainer>
      <Button onClick={() => dispatch(userActions.logout())}>
        Wyloguj
      </Button>
    </>
  );
};

const NavBar = (props) => {
  const { logged, user, dispatch } = props;
  return (
    <Navbar bg="dark" variant="dark" expand="lg">
      <LinkContainer to="/">
        <Navbar.Brand href="#home">Tournament-App</Navbar.Brand>
      </LinkContainer>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {getLoggedContent(logged, user, dispatch)}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

NavBar.propTypes = {
  logged: PropTypes.bool,
  user: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }),
  dispatch: PropTypes.func.isRequired,
};

NavBar.defaultProps = {
  logged: false,
  user: {
    firstName: '',
    lastName: '',
  },
};

function mapStateToProps(state) {
  const { logged, user } = state.login;
  return { user, logged };
}

const connectedNavBar = connect(mapStateToProps)(NavBar);
export default connectedNavBar;
