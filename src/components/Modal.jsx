import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';

const ConfirmationModal = (props) => {
  const {
    show,
    onHide,
    headerText,
    bodyText,
    onClick,
  } = props;
  return (
    <Modal show={show} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title>{headerText}</Modal.Title>
      </Modal.Header>
      <Modal.Body>{bodyText}</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onClick}>
          Ok
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

ConfirmationModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  headerText: PropTypes.string,
  bodyText: PropTypes.string,
};

ConfirmationModal.defaultProps = {
  headerText: '',
  bodyText: '',
};

export default ConfirmationModal;
