import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';

const LoadingModal = (props) => {
  const { show, onHide, text } = props;

  return (
    <Modal show={show} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title>
          <Spinner animation="border" />
          {text}
        </Modal.Title>
      </Modal.Header>
    </Modal>
  );
};

LoadingModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func,
  text: PropTypes.string.isRequired,
};

LoadingModal.defaultProps = {
  onHide: () => {},
};

export default LoadingModal;
