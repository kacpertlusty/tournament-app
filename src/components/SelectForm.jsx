import React from 'react';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import PropTypes from 'prop-types';

const SelectForm = (props) => {
  const {
    description,
    name,
    onChange,
    options,
  } = props;
  return (
    <Form.Group as={Col} controlId="formPlaintextPassword">
      <Form.Label row="true" sm="2" key="description">
        {description}
      </Form.Label>
      <Form.Control sm="10" name={name} as="select" onChange={onChange} key="value">
        {
        options.map(opt => (
          <option key={opt}>
            {opt}
          </option>
        ))
        }
      </Form.Control>
    </Form.Group>
  );
};

SelectForm.propTypes = {
  description: PropTypes.string,
  name: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
};

SelectForm.defaultProps = {
  description: '',
  name: '',
};

export default SelectForm;
