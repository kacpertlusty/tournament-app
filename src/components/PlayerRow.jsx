import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import PropTypes from 'prop-types';

class PlayerRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addScore: 0,
    };
    this.handleScoreChange = this.handleScoreChange.bind(this);
  }

  handleScoreChange(event) {
    this.setState({
      addScore: event.target.value,
    });
  }

  render() {
    const {
      player: playerContainer,
      onClick,
      showActions,
    } = this.props;
    const { addScore } = this.state;
    const { player, score, bonusScore } = playerContainer;
    return (
      <tr>
        <th>{`${player.firstName} ${player.lastName}`}</th>
        <th>{score}</th>
        <th>{bonusScore}</th>
        <th>{score + bonusScore}</th>
        <th>
          {
            showActions
            && (
              <InputGroup>
                <FormControl
                  placeholder="Punkty karne/bonusowe"
                  aria-label="Punkty karne/bonusowe"
                  aria-describedby="basic-addon2"
                  onChange={this.handleScoreChange}
                  value={addScore}
                />
                <InputGroup.Append>
                  <Button onClick={() => onClick(addScore)} variant="outline-secondary">
                    Dodaj do wyniku
                  </Button>
                </InputGroup.Append>
              </InputGroup>
            )
          }
        </th>
      </tr>
    );
  }
}

PlayerRow.propTypes = {
  onClick: PropTypes.func.isRequired,
  player: PropTypes.shape({
    player: PropTypes.shape({
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
    }).isRequired,
    score: PropTypes.number.isRequired,
    bonusScore: PropTypes.number.isRequired,
  }).isRequired,
  showActions: PropTypes.bool,
};

PlayerRow.defaultProps = {
  showActions: false,
};

export default PlayerRow;
