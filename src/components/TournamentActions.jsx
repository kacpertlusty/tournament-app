import React from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { tournaments } from '../redux/actions';
import LoadingModal from './LoadingModal';

const isUserInTournament = (tournament, user) => (
  tournament.players.find(p => p.player._id === user._id)
);

const canDelete = (tournament, user) => tournament.creator._id === user._id;

const TournamentActions = (props) => {
  const {
    dispatch,
    finishingRound,
    tournament,
    user,
    deletingTournament,
  } = props;
  if (!tournament) return (<></>);
  const id = tournament._id;
  const isOwner = canDelete(tournament, user);
  return (
    <ButtonGroup aria-label="StartTournament">
      <LoadingModal
        show={finishingRound}
        onHide={() => {}}
        text="Kończę rundę"
      />
      <LoadingModal
        show={deletingTournament}
        onHide={() => {}}
        text="Usuwam turniej"
      />
      {
        tournament.rounds.length === 0 && isOwner
        && (
          <Button
            key="startTournamentButton"
            variant="primary"
            disabled={tournament.rounds.length}
            onClick={() => dispatch(tournaments.startTournament(id, user.token))}
          >
              Wystartuj turniej
          </Button>
        )
      }
      {
        !isUserInTournament(tournament, user) && tournament.rounds && tournament.rounds.length === 0
        && (
          <Button
            key="joinTournamentButton"
            variant="primary"
            onClick={() => dispatch(push('/joinTournament', tournament._id))}
          >
            Zarejestruj się do turnieju
          </Button>
        )
      }
      {
        isUserInTournament(tournament, user) && tournament.rounds && tournament.rounds.length === 0
        && (
          <Button
            key="unregisterFromTournamentButton"
            variant="primary"
            onClick={() => dispatch(tournaments.unregisterFromTournament(id, user.token))}
          >
            Wyrejestruj się z turnieju
          </Button>
        )
      }
      {
        isOwner
        && (
          <Button
            key="deleteButton"
            onClick={() => dispatch(tournaments.deleteTournament(id, user.token))}
          >
            Usuń turniej
          </Button>
        )
      }
      {
        isOwner
        && (
          <Button
            key="updateButton"
            onClick={() => dispatch(push('/updateTournament', tournament._id))}
          >
            Edytuj
          </Button>
        )
      }
      {
        tournament.rounds.length > 0 && isOwner
        && tournament.rounds.filter(r => !r.finished).length >= 1
        && (
          <>
            <Button
              key="endRoundButton"
              variant="secondary"
              onClick={() => dispatch(
                tournaments.finishRound(id, user.token, tournament.rounds.length),
              )}
            >
              Zakończ rundę
            </Button>
          </>
        )
      }
      {
        tournament.rounds.length === tournament.rounds.filter(r => r.finished).length && isOwner
        && (
          <Button
            key="startNewRoundButton"
            variant="secondary"
            onClick={() => dispatch(tournaments.nextRound(id, user.token))}
          >
            Rozpocznij nową rundę
          </Button>
        )
      }
    </ButtonGroup>
  );
};

TournamentActions.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    token: PropTypes.string,
    id: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  }).isRequired,
  tournament: PropTypes.shape({
    rounds: PropTypes.arrayOf(PropTypes.shape({
      turn: PropTypes.number,
      finished: PropTypes.bool,
      _id: PropTypes.string,
    })),
  }).isRequired,
  finishingRound: PropTypes.bool,
  deletingTournament: PropTypes.bool,
};

TournamentActions.defaultProps = {
  finishingRound: false,
  deletingTournament: false,
};

function mapStateToProps(state) {
  const tournament = state
    .tournaments
    .tournaments
    .filter(t => t._id === state.router.location.state)[0];

  return {
    tournament,
    user: state.login.user,
    finishingRound: state.tournaments.finishingRound,
  };
}

const connectedTournamentActions = connect(mapStateToProps)(TournamentActions);

export default connectedTournamentActions;
