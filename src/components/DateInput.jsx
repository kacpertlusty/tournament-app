/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import DatePicker from 'react-datepicker';

const DateInput = (props) => {
  const {
    date,
    isCalendarOpen,
    toggleCalendar,
    onChange,
    text,
  } = props;
  return (
    <Form.Group as={Row} controlId="formPlaintextPassword">
      <Form.Label column sm="2">
        {text}
      </Form.Label>
      <Col sm="10">
        <Button variant="primary" onClick={toggleCalendar} block>{`${date.toISOString().split('T')[0]} ${date.getHours()}:${date.getMinutes()}`}</Button>
        { isCalendarOpen && (
        <DatePicker selected={date} onChange={onChange} withPortal inline showTimeInput timeInputLabel="Godzina:" />
        )}
      </Col>
    </Form.Group>
  );
};

DateInput.propTypes = {
  date: PropTypes.instanceOf(Date),
  isCalendarOpen: PropTypes.bool,
  toggleCalendar: PropTypes.func,
  onChange: PropTypes.func,
  text: PropTypes.string,
};

DateInput.defaultProps = {
  date: Date.now(),
  isCalendarOpen: false,
  toggleCalendar: () => {},
  onChange: () => {},
  text: '',
};

export default DateInput;
