/* eslint-disable react/prop-types */
import React from 'react';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import PropTypes from 'prop-types';

function FormField(props) {
  const {
    description,
    placeholder,
    value,
    onChange,
    name,
  } = props;
  return (
    <Form.Group as={Row} controlId="formPlaintextPassword">
      <Form.Label column sm="2">
        {description}
      </Form.Label>
      <Col sm="10">
        <Form.Control placeholder={placeholder} value={value} name={name} onChange={onChange} />
      </Col>
    </Form.Group>
  );
}

FormField.propTypes = {
  description: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func,
  name: PropTypes.string,
};

FormField.defaultProps = {
  description: '',
  placeholder: '',
  onChange: () => {},
  name: '',
};

export default FormField;
