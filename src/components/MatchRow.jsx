import React from 'react';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';

const MatchRow = (props) => {
  const { match, onClick } = props;
  const {
    playerA,
    playerB,
    table,
    finished,
  } = match;

  return (
    <tr>
      <th>{playerA && `${playerA.player.firstName} ${playerA.player.lastName} - `}</th>
      <th>{playerA.score}</th>
      {
        playerB.player ? (
          <>
            <th>{playerB.score}</th>
            <th>{playerB && `${playerB.player.firstName} ${playerB.player.lastName}`}</th>
          </>
        ) : (
          <>
            <th>Bye</th>
            <th>Bye</th>
          </>
        )
      }
      <th>{table}</th>
      <th>{finished ? 'Skończony' : 'W toku'}</th>
      <th>
        <Button
          onClick={onClick}
        >
          Szczegóły
        </Button>
      </th>
    </tr>
  );
};

MatchRow.propTypes = {
  match: PropTypes.shape({
    playerA: PropTypes.shape({
      player: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        mmr: PropTypes.number,
      }),
      score: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    }),
    playerB: PropTypes.shape({
      player: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        mmr: PropTypes.number,
      }),
      score: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    }),
    table: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    finished: PropTypes.bool,
  }).isRequired,
  onClick: PropTypes.func,
};

MatchRow.defaultProps = {
  onClick: () => {},
};

export default MatchRow;
