import PropTypes from 'prop-types';
import React from 'react';
import Table from 'react-bootstrap/Table';

const RoundRow = (props) => {
  const { round } = props;
  return (
    <>
      <h1>{`Runda ${round.turn}`}</h1>
      <Table responsive="sm">
        <thead>
          <tr>
            <th>Stół</th>
            <th>Gracz A</th>
            <th>MMR</th>
            <th>Wynik gracza A</th>
            <th>Wynik gracza B</th>
            <th>MMR</th>
            <th>Gracz B</th>
          </tr>
        </thead>
        <tbody>
          {
            round.matches.map(match => (
              // eslint-disable-next-line no-underscore-dangle
              <tr key={match._id}>
                <th>{match.table}</th>
                <th>{`${match.playerA.player.firstName} ${match.playerA.player.lastName}`}</th>
                <th>{match.playerA.player.mmr}</th>
                <th>{match.playerA.score}</th>
                {
                  match.playerB.player ? (
                    <>
                      <th>{match.playerB.score}</th>
                      <th>{match.playerB.player.mmr}</th>
                      <th>{`${match.playerB.player.firstName} ${match.playerB.player.lastName}`}</th>
                    </>
                  )
                    : (
                      <>
                        <th>Bye</th>
                        <th>Bye</th>
                        <th>Bye</th>
                      </>
                    )
                }
              </tr>
            ))
          }
        </tbody>
      </Table>
    </>
  );
};

RoundRow.propTypes = {
  round: PropTypes.shape({
    matches: PropTypes.arrayOf(PropTypes.shape({
      playerA: PropTypes.shape({
        player: PropTypes.shape({
          firstName: PropTypes.string,
          lastName: PropTypes.string,
          mmr: PropTypes.number,
        }),
        score: PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.number,
        ]),
      }),
      playerB: PropTypes.shape({
        player: PropTypes.shape({
          firstName: PropTypes.string,
          lastName: PropTypes.string,
          mmr: PropTypes.number,
        }),
        score: PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.number,
        ]),
      }),
      table: PropTypes.number,
    })).isRequired,
    turn: PropTypes.number,
  }).isRequired,
};

export default RoundRow;
