import React from 'react';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';

const TournamentRow = (props) => {
  const { tournament, onClick } = props;
  const {
    name,
    description,
    city,
    players,
    maxPlayers,
    rank,
  } = tournament;

  return (
    <tr key={name}>
      <th>{name}</th>
      <th>{description}</th>
      <th>{city}</th>
      <th>{`${players.length} / ${maxPlayers}`}</th>
      <th>{rank}</th>
      <th><Button onClick={onClick}>Wyświetl</Button></th>
    </tr>
  );
};

TournamentRow.propTypes = {
  tournament: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    players: PropTypes.array.isRequired,
    maxPlayers: PropTypes.number.isRequired,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default TournamentRow;
