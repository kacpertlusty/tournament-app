/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-first-prop-new-line */
/* eslint-disable react/jsx-max-props-per-line */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const PrivateRouteComponent = (props) => {
  const {
    routeProps,
    children,
    location,
    logged,
  } = props;
  return (
    <Route {...routeProps} render={() => (
      logged ? (
        <div>{children}</div>
      ) : (
        <Redirect to={{
          pathname: '/login',
          state: { from: location },
        }}
        />
      )
    )}
    />
  );
};

PrivateRouteComponent.propTypes = {
  logged: PropTypes.bool.isRequired,
  location: PropTypes.string.isRequired,
  routeProps: PropTypes.shape({
    exact: PropTypes.bool.isRequired,
    path: PropTypes.string.isRequired,
  }).isRequired,
  children: PropTypes.element.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  logged: state.login.logged,
  location: ownProps.path,
  routeProps: {
    exact: ownProps.exact,
    path: ownProps.path,
  },
});

const PrivateRoute = connect(mapStateToProps, null)(PrivateRouteComponent);
export default PrivateRoute;
