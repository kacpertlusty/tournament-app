/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import * as Pages from './pages';
import { PrivateRoute, NavBar } from './components';
import store, { history } from './redux/store';
import * as serviceWorker from './serviceWorker';

import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <>
        <NavBar />
        <Switch>
          <PrivateRoute exact path="/">
            <Pages.MainPage />
          </PrivateRoute>
          <PrivateRoute exact path="/createTournament">
            <Pages.CreateTournament />
          </PrivateRoute>
          <PrivateRoute exact path="/showTournaments">
            <Pages.ShowTournaments />
          </PrivateRoute>
          <PrivateRoute exact path="/tournamentDetails">
            <Pages.TournamentDetails />
          </PrivateRoute>
          <PrivateRoute exact path="/activeGame">
            <Pages.ActiveGame />
          </PrivateRoute>
          <PrivateRoute exact path="/showMatches">
            <Pages.MatchHistory />
          </PrivateRoute>
          <PrivateRoute exact path="/joinTournament">
            <Pages.JoinTournament />
          </PrivateRoute>
          <PrivateRoute exact path="/matchDetails">
            <Pages.MatchDetails />
          </PrivateRoute>
          <PrivateRoute exact path="/updateTournament">
            <Pages.UpdateTournament />
          </PrivateRoute>
          <PrivateRoute exact path="/playerRanking">
            <Pages.PlayerRanking />
          </PrivateRoute>
          <PrivateRoute exact path="/playerDetails">
            <Pages.PlayerDetails />
          </PrivateRoute>
          <Route exact path="/login" render={() => (<Pages.Login />)} />
        </Switch>
      </>
    </ConnectedRouter>
  </Provider>,
  // eslint-disable-next-line no-undef
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
